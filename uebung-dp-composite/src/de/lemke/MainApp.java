package de.lemke;

import de.lemke.composite.*;

public class MainApp
{
  public static void main(String [] args)
   {
     //einige Beispielfahrzeuge
     Fahrzeug f1 = new Fahrzeug("F1");
     Fahrzeug f2 = new Fahrzeug("F2");
     Fahrzeug f3 = new Fahrzeug("F3");
     Fahrzeug f4 = new Fahrzeug("F4");

     //dem Standort s1 werden die Fahrzeuge f1 und f2 zugeordnet
     Standort s1 = new Standort("S1");
     s1.add(f1);
     s1.add(f2);

     //der Standort s2 besteht aus Standort s1 und den Fahrzeugen f3 und f4
     Standort s2 = new Standort("S2");
     s2.add(f3);
     s2.add(f4);
     s2.add(s1);

     //den Standort s1 ausgeben
     s1.toConsole();

     System.out.println("-------------------------------------------");

     //den Standort s2 ausgeben, der den Standort s1 beinhaltet
     s2.toConsole();
   }
}
