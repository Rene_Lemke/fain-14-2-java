package de.lemke.composite;

import java.util.*;

public class Standort implements Verleih
{
  private String name;
  private List<Verleih> liste;

  public Standort(String n)
   {
     this.name = n;
     this.liste = new ArrayList<Verleih>();
   }

  //Elemente zum Standort hinzufügen (Fahrzeuge oder Standorte)
  public void add(Verleih v)
   {
     this.liste.add(v);
   }

  @Override
  public void toConsole()
   {
     System.out.println(this.getClass().getSimpleName() + ": " + this.name);

     for(Verleih v: this.liste)
       {
         v.toConsole();
       }
   }

  @Override
  public void toFile()
  {
    
  }
}
