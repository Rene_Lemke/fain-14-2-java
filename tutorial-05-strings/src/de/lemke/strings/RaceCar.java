package de.lemke.strings;

public class RaceCar extends AbstractCar
{
  public RaceCar(int km)
    {
      this.mileage = km;
    }

  @Override
  public void move(int km)
    {
      if( km < 0 )
            return;
      this.mileage += km;
    }


  @Override
  public String toString()
    {
      return String.format("%s mit %d km auf dem Tacho.", super.toString(), this.mileage);  //zweiter Teil der Ausgabe
    }
}
