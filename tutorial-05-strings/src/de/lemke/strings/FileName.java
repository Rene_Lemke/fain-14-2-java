public class FileName
{
 private String path;
 private char pathSeparator;
 private char extSeparator;

 /**
 * Konstruktor
 *
 * @param p                 vollständige Pfadangabe (Pfad + Datei)
 * @param sep               das Trennzeichen zwischen den Verzeichnissen
 * @param ext               das Trennzeichen zwischen Datei und Erweiterung
 */

 public FileName(String p, char sep, char ext)
     {
      this.path = p;
      this.pathSeparator = sep;
      this.extSeparator = ext;
     }

 /**
 *  liefert den Dateinamen ohne Erweiterung
 *
 * @return Dateinamen*/
 public String getFileName()
    {
     int extPos = this.path.lastIndexOf(this.extSeparator);
     int sepPos = this.path.lastIndexOf(this.pathSeparator);

     return this.path.substring(sepPos+1, extPos);
    }

 /**
 *   liefert den vollständigen Pfad ohne Dateinamen
 *
 *   @return Pfadangabe
 */
 public String getPath()
   {
     int sepPos = this.path.lastIndexOf(this.pathSeparator);

     return this.path.substring(0, sepPos);
   }
}
