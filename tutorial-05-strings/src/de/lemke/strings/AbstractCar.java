package de.lemke.strings;

public abstract class AbstractCar
{
  protected int mileage = 0;

  public abstract void move(int km);

  @Override
  public String toString()
     {
       return String.format("Ich bin ein Objekt der Klasse: %s", this.getClass().getSimpleName());  //erster Teil der Ausgabe
     }
}
