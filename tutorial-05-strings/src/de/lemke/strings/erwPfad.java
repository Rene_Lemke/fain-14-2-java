package de.lemke.strings;

public class erwPfad
{
 private static final String ANSI_RESET = "\u001B[0m";
 private static final String ANSI_RED = "\u001B[31m";

 public static void main(String [] args)
     {
       if(args.length == 0)
          {
            System.out.println("usage: de.lemke.string.erwPfad <pfad>");
            return;
          }
       String [] dirs;
       String [] file;

       if( args[0].startsWith("/"))
            dirs = args[0].substring(1).split("/");
       else
          dirs = args[0].split("/");

      //den letzten Eintrag aus dem gesplitteten Pfad holen
      String tmp = dirs[dirs.length-1];

      file = tmp.split("\\.");                   // der doppelte \ damit der Punkt gequotet wird

      System.out.format(ANSI_RED + "[%s]" + ANSI_RESET + "wurde gesplittet in: %n", args[0]);

      int i=0;

      for(String d: dirs)
         {
           System.out.format("%02d. %s %n", i++, d);
         }

      for(String f: file)
         {
           System.out.format("%02d. %s %n", ++i, f);
         }
     }
}
