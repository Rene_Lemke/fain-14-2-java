package de.lemke.enums;

//Planeten mit (Masse,Radius)
public enum Planet
{
  MERKUR    (3.303e+23, 2.4397e6  ),
  VENUS     (4.869e+24, 6.0518e6  ),
  ERDE      (5.976e+24, 6.37814e6 ),
  MARS      (6.421e+23, 3.3972e6  ),
  JUPITER   (1.9e+27,   7.1492e7  ),
  SATURN    (5.688e+26, 6.0268e7  ),
  URANUS    (8.686e+25, 2.5559e7  ),
  NEPTUN    (1.024e+26, 2.4746e7  );

  private final double masse;         //Masse in kg
  private final double radius;        //Radius in m

  //Konstruktor der Enumeration
  Planet(double masse, double radius)
    {
      this.masse  = masse;
      this.radius = radius;
    }

  protected double getMasse()
    {
      return this.masse;
    }

  protected double getRadius()
    {
      return this.radius;
    }

  //universale Gravitationskonstante m * kg^-1 * s⁻-2
  public static final double G = 6.67300e-11;

  double surfaceGravity()
    {
       return ( G * this.masse ) / ( this.radius * this.radius );
    }

  double surfaceWeight(double andereMasse)
    {
      return andereMasse * this.surfaceGravity();
    }
}
