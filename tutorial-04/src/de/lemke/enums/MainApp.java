package de.lemke.enums;

public class MainApp
{
    Day tag;

    //Konstruktor der Klasse erwartet einen Aufzählungswert aus Day, z.B. Day.MONDAY
    public MainApp(Day d)
      {
        this.tag = d;
      }

   // Die Methode kommentiert den einzelnen Wochentag
   public void kommentiereDenTag()
    {
       switch(this.tag)
          {
            case MONDAY:
                System.out.println("Ich hasse Montage!");
                break;
            case WEDNESDAY:
                System.out.println("Bergfest!");
                break;
            case FRIDAY:
                System.out.println("Wochenende in Sicht!");
                break;
            case SATURDAY:
            case SUNDAY:
                System.out.println("Die schönste Zeit.");
                break;
            default:
                System.out.println("so lala");
                break;
          }
    }

    public static void main(String [] args)
      {
          MainApp montag = new MainApp(Day.MONDAY);
          montag.kommentiereDenTag();

          MainApp dienstag = new MainApp(Day.TUESDAY);
          dienstag.kommentiereDenTag();

          MainApp mittwoch = new MainApp(Day.WEDNESDAY);
          mittwoch.kommentiereDenTag();

          MainApp samstag = new MainApp(Day.SATURDAY);
          samstag.kommentiereDenTag();

         //hier würde ein Fehler erzeugt werden da der Datentyp nicht mit den enums zusammenpasst
/*          MainApp err = new MainApp(1);
            err.kommentiereDenTag();
*/


/*
*    -------zweites Beispiel zu den Enums - Planeten --------
*/
         double gewichtErde = 75.0;
         double masse = gewichtErde / Planet.ERDE.surfaceGravity();

         for( Planet p: Planet.values() )
            {
//              System.out.println("Das Gewicht auf " + p + " ist " + p.surfaceWeight(masse) + " kg. ");
                System.out.printf("Das Gewicht auf %s ist %4.2f kg. %n", p, p.surfaceWeight(masse));
            }
      }
}
