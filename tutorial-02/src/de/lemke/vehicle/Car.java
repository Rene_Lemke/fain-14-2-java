package de.lemke.vehicle;

public class Car
{
  @Override                      //sieht in der Oberklasse nach ob es nachfolgende Methode überhaupt gibt
  public String toString()
    {
      return this.getClass().getSimpleName() + "["+this.hashCode() +"]: brumm brumm";   //fluent interface
    }
}
