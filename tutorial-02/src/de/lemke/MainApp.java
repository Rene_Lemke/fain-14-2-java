package de.lemke;
import de.lemke.vehicle.*;
import de.lemke.fuhrpark.*;

class MainApp
{
  public static void main(String [] args)
     {
       Greeter greeter = new Greeter();
       greeter.sayHello();

       //wir bauen uns ein Auto
       Car c = new Car();

       System.out.println(c);

       //Garage mit Autos
       Garage g = new Garage(10);

       g.addCar(new Car());
       g.addCar(new Car());
       g.addCar(new Car());
       g.addCar(new Car());
       g.addCar(new Car());
       g.addCar(new Car());
       g.addCar(new Car());

       g.writeList();
     }
}
