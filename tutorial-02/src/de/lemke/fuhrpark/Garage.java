package de.lemke.fuhrpark;

import de.lemke.vehicle.Car;

public class Garage
{
  Car [] stellplatz;

  public Garage(int count)
    {
      this.stellplatz = new Car[count];
    }

 // platziert ein Auto auf einem freien Stellplatz
 public void addCar(Car c)
    {
     int i;
     for(i=0;i<this.stellplatz.length;i++)
       {
        if(this.stellplatz[i]==null)
          break;
       }

     if(i<this.stellplatz.length)
        this.stellplatz[i]=c;
    }

 // gibt alle Autos der Garage aus
 public void writeList()
   {
     for(Car c: this.stellplatz)
       {
         if(c!=null)
             System.out.println(c);
         else
          break;
       }
   }
}
