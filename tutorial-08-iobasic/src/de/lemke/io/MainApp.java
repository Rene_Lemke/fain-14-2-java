package de.lemke.io;

import java.io.IOException;

public class MainApp
{
 public static void main(String [] args) throws IOException
   {
    //byteweise Kopieren
    CopyByteStream cbs = new CopyByteStream();
    cbs.copy(args[0], args[1]);

    //zeichenweise Kopieren
    CopyCharacterStream ccs = new CopyCharacterStream();
    ccs.copy(args[0], args[2]);

    //zeilenweise (gepuffert)
    CopyLineStream cls = new CopyLineStream();
    cls.copy(args[0], args[3]);

    //tokenweise
    CopyScanner cs = new CopyScanner();
    cs.copy(args[0]);

   }
}
