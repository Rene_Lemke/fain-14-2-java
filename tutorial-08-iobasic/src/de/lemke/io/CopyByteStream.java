package de.lemke.io;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.FileNotFoundException;

public class CopyByteStream
{
  public void copy(String input, String output) throws IOException
     {
      FileInputStream in = null;
      FileOutputStream out = null;

      try
       {
        //ggfs. wird eine FileNotFoundException geworfen
        in = new FileInputStream(input);
        out = new FileOutputStream(output);

        int c;
        while((c=in.read()) != -1)               // -1 entspricht 'end of file' Zeichen
          {
            out.write(c);                        // wir schreiben das eingelesene Byte in den FileOutputStream
          }
       }
       catch(FileNotFoundException e)
         {
           System.out.println(e);
         }
      finally
         {
           //close() wirft ggfs. eine IOException
           if( in != null )
             {
               in.close();
             }
           if(out != null)
             {
               out.close();
             }
         }
     }
}
