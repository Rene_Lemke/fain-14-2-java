package de.lemke.io;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.FileNotFoundException;

public class CopyCharacterStream
{
  public void copy(String input, String output) throws IOException
    {
      FileReader in = null;
      FileWriter out = null;

    try
      {
       in = new FileReader(input);
       out = new FileWriter(output);

       int c;

      while((c = in.read()) != -1)             //read() liefert dieses Mal einen 16-Bit Wert
       {
         out.write(c);
       }
      }
    catch(FileNotFoundException e)
     {
      System.out.println(e);
     }
   finally
     {
      // ggfs. wirft close() die IOException
      if( in != null)
        {
          in.close();
        }
      if(out != null)
        {
          out.close();
        }
      }
   }
}
