package de.lemke.io;

import java.io.FileReader;
import java.io.BufferedReader;
                                             //Ausgabe erfolgt hier auf Kommandozeile
import java.io.IOException;
import java.io.FileNotFoundException;

import java.util.Scanner;

public class CopyScanner
{
  public void copy(String input) throws IOException
    {
      Scanner scan = null;

     try
      {
        scan = new Scanner(new BufferedReader(new FileReader(input)));

        while(scan.hasNext())
          {
            //wir holen uns das nächste Token aus dem Stream, Scanner zerlegt den Stream anhand der Trennzeichen in Token
            System.out.println(scan.next());
          }
      }
     catch(FileNotFoundException e)
        {
         System.out.println(e);
        }
     finally
        {
          if(scan != null)
            {
              // der Aufruf close() wird an das injizierte BufferedReader-Objekt delegiert
              scan.close();
            }
        }
     }
}
