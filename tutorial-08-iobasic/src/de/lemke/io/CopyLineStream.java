package de.lemke.io;

import java.io.FileReader;
import java.io.FileWriter;

import java.io.BufferedReader;                    //Pufferklassen
import java.io.PrintWriter;

import java.io.FileNotFoundException;
import java.io.IOException;

public class CopyLineStream
{
  public void copy(String input, String output) throws IOException
    {
      BufferedReader in = null;
      PrintWriter out = null;

      try
        {
          //wir erzeugen zunächst die FileReader und FileWriter-Objekte und injizieren diese in die gepufferten Klassen BufferedReader
          //und PrintWriter (dependency injection)
          in = new BufferedReader(new FileReader(input));
          out = new PrintWriter(new FileWriter(output));

          String line;
          int nummer=1;

          while((line = in.readLine()) != null)                    //alternativ: for(int nummer=1, (line=in.readLine()) != null, nummer++)
             {
               out.println("Zeile " + nummer + ": " + line);
               nummer++;
             }
        }
     catch(FileNotFoundException e)
        {
         System.out.println(e);
        }
     finally
        {
          if(in != null)
            {
              in.close();
            }
          if(out != null)
            {
              out.close();
            }
        }
    }
}
