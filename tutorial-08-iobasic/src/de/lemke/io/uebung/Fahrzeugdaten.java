package de.lemke.io.uebung;

import java.io.FileReader;
import java.io.BufferedReader;

import java.io.FileNotFoundException;
import java.io.IOException;

public class Fahrzeugdaten
{
  public void copy(String input) throws IOException
    {
     BufferedReader in = null;

     try
       {
         in = new BufferedReader(new FileReader(input));

         String data;

         System.out.format("%15s|%15s|%15s|%15s%n","Hersteller","Farbe","Kilometerstand","Leistung");
         System.out.format("---------------+---------------+---------------+---------------%n");
         while((data = in.readLine()) != null)
           {
             String [] teile = data.split(",");
             System.out.format("%15s|%15s|%15s|%15s%n", teile[0],teile[1],teile[2],teile[3]);
           }
       }
       catch(FileNotFoundException e)
        {
          System.out.println(e);
        }
      finally
        {
          if(in != null)
            {
              in.close();
            }
        }

    }
}
