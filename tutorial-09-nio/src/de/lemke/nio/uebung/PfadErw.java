package de.lemke.nio.uebung;

import java.nio.file.*;
import java.io.*;
import java.util.*;

public class PfadErw
{
	// erzeugt einen Pfad auf Basis einer String-Variable
	public Path erzeuge(String name)
	{
		return Paths.get(name);
	}

	// Informationen zu Pfad ausgeben
	public void infoPfad(Path pfad)
	{
		// Pfad ausgeben
		System.out.format("%-30s%s%n", "Pfad:", pfad );

		// Pfadseparator ausgeben
		System.out.format("%-30s%s%n",
			"Trennzeichen für Pfadangabe:", File.separator);

		// getFileName() liefert den tetzten Teil des Pfades
		System.out.format("%-30s%s%n",
			"Dateiname:", pfad.getFileName() );

		// getNameCount() liefert die Anzahl der Namensbestandteile
		System.out.format("%-30s%s%n",
			"Anzahl Namensbestandteile:", pfad.getNameCount() );

		// getParent() liefert das Elternverzeichnis
		System.out.format("%-30s%s%n",
			"Elternverzeichnis:", pfad.getParent() );

		// getRoot() liefert das Wurzelverzeichnis
		System.out.format("%-30s%s%n",
			"Wurzelverzeichnis:", pfad.getRoot() );
	}

	// Weitere Informationen zum Pfad ausgeben
	public void analysieren(Path pfad)
	{
		boolean isRegularFile	= Files.isRegularFile(pfad);
		boolean isDirectory		= Files.isDirectory(pfad);
		boolean isReadable		= Files.isReadable(pfad);
		boolean isWritable		= Files.isWritable(pfad);
		boolean isExecutable	= Files.isExecutable(pfad);
		String file = pfad.getFileName().toString();

		System.out.format("'%s' ist eine reguläre Datei: %b%n",
			file, isRegularFile);

		System.out.format("'%s' ist ein Verzeichnis: %b%n",
			file, isDirectory);

		System.out.format("'%s' [rwx] = %b %b %b%n",
			file, isReadable, isWritable, isExecutable );
	}

	// Namesbestandteile zeilenweise ausgeben
	public void namensbestandteile(Path pfad)
	{
		int anzahl = pfad.getNameCount();

		System.out.println("NAMENSBESTANDTEILE:");
		for (int i = 0; i < anzahl; i++) {
			System.out.format("%4s%s%n", " ", pfad.getName(i) );
		}
	}

	// name ist eine Zusammensetzung aus Pfad und Datei
	public void erzeugePfad(String name)
	{
		// wir erzeugen uns ein Pfadobjekt
		Path pfad = Paths.get(name);

		// wir holen uns das Elternverzeichnis
		Path verzeichnis = pfad.getParent();

		// wir prüfen, ob das Verzeichnis existiert
		if ( Files.isDirectory(verzeichnis) ) {

			try {
				// ... dann die Datei anlegen
				Files.createFile(pfad);
			}
			catch(IOException e) {
				System.out.println(e);
			}

		}
	}

	/*
	 * legt eine Datei an, eine evtl. nicht vorhandene Dateistruktur wird
	 * mit angelegt
	 */
   ArrayList<Path> directoryList;

	 public void Pfad()
 	{
 		directoryList = new ArrayList<>();
 	}

	public void createData(Path pfad)
	{
		try {
			// Verzeichnisstruktur anlegen
			Files.createDirectories( pfad.getParent() );

			// Datei anlegen
			Files.createFile(pfad);
		}
		catch (IOException e) {
			System.out.println(e);
		}
	}

	// Verzeichnisse sortiert ausgeben
	public void listSortedDirectoy(Path pfad, boolean recursive)
	{
		directoryList.clear();

		if (recursive) {
			this.createRecursiveDirectoryList(pfad);
		}
		else {
			this.createDirectoryList(pfad);
		}

		Collections.sort(directoryList);

		for (Path entry : this.directoryList) {
			System.out.println(entry);
		}
	}

	// erstellt eine Verzeichnisliste
	private void createDirectoryList(Path pfad)
		{
			try (DirectoryStream<Path> ds = Files.newDirectoryStream(pfad) ){
				for (Path entry : ds) {
					this.directoryList.add(entry);
				}
			}
			catch (IOException e) {
				System.out.println(e);
			}
		}


	// Listet alle Verzeichniseinträge rekursiv auf
	private void createRecursiveDirectoryList(Path pfad)
		{
			try (DirectoryStream<Path> ds = Files.newDirectoryStream(pfad) ) {
				for (Path entry : ds) {
					this.directoryList.add(entry);

					if ( Files.isDirectory(entry) ) {
						createRecursiveDirectoryList(entry);
					}
				}
			}
			catch (IOException e) {
				System.out.println(e);
			}
		}
}
