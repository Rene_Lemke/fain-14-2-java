package de.lemke.nio.uebung;

import java.nio.file.*;
import java.io.*;

public class UebungApp
{
	public static void main(String[] args)
	{
		PfadErw pf = new PfadErw();

		Path pfad1 = Paths.get("/home/lemke/Dokumente");
		Path pfad2 = pf.erzeuge("/var/log/apache2");
		Path pfad3 = Paths.get("/home/lemke/Test" +
			"/nio/MainApp.java");

		// liste alle Verzeichnisse im angebenen Verzeichnis auf
		Path pfad4 = Paths.get("/home/lemke");
		System.out.println();
		pf.listSortedDirectory(pfad4, false);

		// liste alle Verzeichnisse rekursiv auf
		Path pfad5 = Paths.get("/home/lemke/Dokumente/Test");
		System.out.println();
		pf.listSortedDirectory(pfad5, true);
	}
}
