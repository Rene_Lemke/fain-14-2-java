package de.lemke.nio;

import java.nio.file.*;
import java.io.File;
import java.io.IOException;

public class Pfad
{
  protected Path pfad1 = Paths.get("/home/lemke");
  protected Path pfad2;
  protected Path pfad3 = Paths.get("C:/windows/system32/drivers/etc/hosts");

  // erzeugt einen Pfad auf Basis einer String-Variable
  public void erzeuge(String name)
    {
      pfad2 = Paths.get(name);
    }

  public void ausgeben()
    {
      System.out.println("Pfad1: " + pfad1);
      System.out.println("Pfad2: " + pfad2);

      System.out.println("Zum Trennen der Pfadangaben wird das folgende Zeichen verwendet: " + File.separator);
    }

  public void infoPfad1()
    {
      //getFileName liefert den letzten Teil des Pfades
      System.out.println("getFileName(): " + pfad1.getFileName());

      //getNameCount() liefert die Anzahl der Namensbestandteile
      System.out.println("getNameCount(): " + pfad1.getNameCount());

      // getParent() liefert das Elternverzeichnis
      System.out.println("getParent(): " + pfad1.getParent());

      //getRoot() liefert das Wurzelverzeichnis (unter Linux "/", unter Windows c:\)
      System.out.println("getRoot(): " + pfad1.getRoot());
    }

/*
*
  für Windows:

  public void infoPfad3()
    {
          //getFileName liefert den letzten Teil des Pfades
          System.out.println("getFileName(): " + pfad3.getFileName());

          //getNameCount() liefert die Anzahl der Namensbestandteile
          System.out.println("getNameCount(): " + pfad3.getNameCount());

          // getParent() liefert das Elternverzeichnis
          System.out.println("getParent(): " + pfad3.getParent());

          //getRoot() liefert das Wurzelverzeichnis (unter Linux "/", unter Windows c:\)
          System.out.println("getRoot(): " + pfad3.getRoot());
    }
*/

      public void analysieren()
        {
          //Files ist eine Klasse aus den NIO-Framework
          boolean pfad1_isRegularFile = Files.isRegularFile(pfad1);
          boolean pfad1_isDirectory = Files.isDirectory(pfad1);

          boolean pfad1_isReadable = Files.isReadable(pfad1);
          boolean pfad1_isWritable = Files.isWritable(pfad1);
          boolean pfad1_isExecutable = Files.isExecutable(pfad1);

          System.out.format("%s ist eine reguläre Datei: %b %n", pfad1, pfad1_isRegularFile);

          System.out.format("%s ist ein Verzeichnis: %b %n", pfad1, pfad1_isDirectory);

          System.out.format("%s [rwx] = %b, %b, %b %n", pfad1, pfad1_isReadable, pfad1_isWritable, pfad1_isExecutable);
        }

    // name ist eine Zusammensetzung aus Pfad und Datei
    public void erzeugePfad(String name)
      {
        //erzeugen eines Pfadobjektes
        Path meinPfad = Paths.get(name);

        //holen des Elternverzeichnisses
        Path meinVerzeichnis = meinPfad.getParent();

        //holen des Dateinamen
        Path meineDatei = meinPfad.getFileName();

        //prüfen, ob das Verzeichnis existiert
        if(Files.isDirectory(meinVerzeichnis))
         {
           try
            {
             //...dann Datei anlegen
             Files.createFile(meinPfad);
            }
           catch(IOException e)
            {
              System.out.println(e);
            }
         }
      }
}
