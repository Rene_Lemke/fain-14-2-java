package de.lemke.gui;

import javax.swing.*;
import java.awt.event.*;
import java.text.NumberFormat;

public class AppWindow extends JFrame
{
    JTextField textfield_dm;
    JTextField textfield_euro;

    JButton button;


    public AppWindow()
     {
       this.getContentPane().setLayout(null);

       this.initWindow();

       this.addWindowListener(new WindowListener()
         {
             @Override
             public void windowClosed(WindowEvent arg0)
             {
             }

             @Override
             public void windowActivated(WindowEvent e)
             {
             }

             @Override
             public void windowClosing(WindowEvent e)
             {
                 System.exit(0);
             }

             @Override
             public void windowDeactivated(WindowEvent e)
             {
             }

             @Override
             public void windowDeiconified(WindowEvent e)
             {
             }

             @Override
             public void windowIconified(WindowEvent e)
             {
             }

             @Override
             public void windowOpened(WindowEvent e)
             {
             }
         }
       );
     }


    protected void initWindow() {
        //Instanzieren:
        textfield_dm = new JTextField();
        textfield_euro = new JTextField();

        //hier wird die Beschriftung des Buttons mit angegeben:
        button = new JButton("DM in Euro");

        button.addActionListener(new ActionListener()
        {
            public void actionPerformed (ActionEvent arg0)
            {
                buttonBerechneClicked();
            }
        }
        );

        //die Methode setBounds(...) nimmt vier int-Parameter entgegen: x-Position, y-Position, Breite und Höhe
        textfield_dm.setBounds(5, 10, 200, 25);
        textfield_euro.setBounds(5, 50, 200, 25);

        button.setBounds(95, 110, 110, 30);


        //Elemente dem Fenster hinzufügen:
        this.getContentPane().add(textfield_dm);
        this.getContentPane().add(textfield_euro);
        this.getContentPane().add(button);

        this.pack();
       }

    public void buttonBerechneClicked()
    {
        double dm = 0;

        //Hole Zahl aus Textfeld:
        try  {
               dm = Double.parseDouble(textfield_dm.getText());
             }
        catch (NumberFormatException e)
        {
            dm = -1;
        }

        //falls n ok ist
        if(dm >= 0)
        {
            //rechne:
            double euro = this.dm2euro(dm);

            //Packe a in Ausgabefeld:
            NumberFormat nf = NumberFormat.getInstance();

            nf.setMaximumFractionDigits(2);

            String ausgabe = nf.format(euro);

            textfield_euro.setText(ausgabe);
        }
        else
        {
            textfield_euro.setText("Eingabe ist nicht in Ordnung!");
        }
    }

    public double dm2euro(double dm)
    {
        double euro = (dm / 1.95583);

        return euro;
    }
}
