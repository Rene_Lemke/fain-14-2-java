package de.lemke.io;

import java.nio.file.*;
import java.io.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import java.util.StringTokenizer;


/**
 * Aufgabe 3: Formatiert den Text aus der angegebenen Datei im Blocksatz und gibt ihn wieder auf der
 * Konsole aus. Die Breite einer Zeile ist in der Konstante LINELENGTH festgelegt.
 */
class Blocksatz {
    /** Länge der Zeile, für die zu formatieren ist */
    public static final int LINELENGTH=60;

    /**
     * Stellt die Länge des Strings fest, wobei mehrfach auftretende bestimmte Zeichen nicht
     * mitgezählt werden.
     *
     * lineIn = der String dessen Länge festzustellen ist
     * c das Zeichen, das bei mehrfachem Auftreten nur einmal gezählt wird
     *
     */
    private static int zaehle(char[] lineIn,char c) {
        int i;
        int j=0;
        for(i=0;i < lineIn.length;i++) {
            if(lineIn[i]==c) {
                while((++i < lineIn.length) && lineIn[i]==c) {
                    j++;
                }
            }
        }
        return i-j;                       // gibt die Länge des Strings zurück
    }

    /**
     * Liest den Text von der Datei ein und gibt in als Blocksatz formatiert aus.
     */
    public static void main(String[] args) throws IOException {

        FileReader fr = new FileReader("xanadu.txt");           //Einlesen der Datei
        BufferedReader in=new BufferedReader(fr);

        int[] wc=new int[2];
        int leerzeichenTotal;
        int leerzeichenEingefuegt;
        int leerzeichenAddiert;

        String[] lineIn=new String[2];
        String line;
        StringBuffer lineOut;
        StringTokenizer[] worte=new StringTokenizer[2];

        int a=1;
        int aa=0;

        line=in.readLine();
        lineIn[0]=line.trim();

        worte[0]=new StringTokenizer(lineIn[0]," ");
        wc[0]=worte[0].countTokens();

        // Jeweils eine Zeile voraus lesen
        while((line=in.readLine())!=null) {

            lineIn[a]=line.trim();
            worte[a]=new StringTokenizer(lineIn[a]," ");
            wc[a]=worte[a].countTokens();

            if(wc[aa] != 0) {
                /* nächste Zeile ist nicht leer, ==> Blocksatz für lineIn[aa] */
                if(wc[aa] > 1) {
                    leerzeichenTotal=LINELENGTH-zaehle(lineIn[aa].toCharArray(),' ');
                    leerzeichenEingefuegt=leerzeichenTotal/(wc[aa]-1);
                    leerzeichenEingefuegt++;
                    leerzeichenAddiert=leerzeichenTotal%(wc[aa]-1);

                    lineOut=new StringBuffer(worte[aa].nextToken());

                    for(int i=1;i < wc[aa];i++) {
                        for(int j=0;j < leerzeichenEingefuegt;j++) {
                            lineOut.append(" ");
                        }
                        if(leerzeichenAddiert-->0) {
                            lineOut.append(" ");
                        }
                        lineOut.append(worte[aa].nextToken());
                    }
                    System.out.println(lineOut);
                } else { /* wenn wc[aa]==0: Leerzeile: übernehmen */
                    System.out.println(lineIn[aa]);
                }
            } else { /* wenn wc[a]==0: Leerzeile folgt: kein Blocksatz für die letzte Zeile */
                System.out.println(lineIn[aa]);
            }
            /* nächste Zeile einlesen: Indizes tauschen */
            a++;
            aa++;
            a%=2;
            aa%=2;
        }

        try
        {
            fr.close();
        }
        catch (Exception e)
        {
            System.out.println(e.getMessage());
            System.out.println("Schreibfehler!");
        }

        /* letzte Zeile: kein Blocksatz */
        System.out.println(lineIn[aa]);
        return;

    }
}
