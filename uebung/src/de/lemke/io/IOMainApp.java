package de.lemke.io;

import java.nio.file.*;
import java.io.*;
import java.io.IOException;
import java.lang.*;

public class IOMainApp
{
    private static String Aufgabe1 = "Aufgabe 1 - Einlesen einer Textdatei und Ausgabe des Inhaltes in Grossbuchstaben";

    /*
    *   Einlesen der Textdatei als kompletter Dateiname, evtl. mit Pfad als Parameter
    */
    public FileInputStream txtInFileOpen(String datei)
    {
        FileInputStream inFileStream = null;                         //Eingabe
        File inFile = null;                                          //Datei

        inFile = new File(datei);

        //FileNotFoundException abfangen
        try
        {
            inFileStream = new FileInputStream(inFile);
        }
        catch (Exception e)
        {
            System.out.println(e.getMessage());
            System.out.println("Die Datei konnte nicht eingelesen werden!");
        }
        return inFileStream;
    }

    /*
    *    Öffnen und Speichern der Textdatei, wenn noch nicht vorhanden wird sie neu angelegt, vorhandene überschrieben.
     */
    public FileOutputStream txtOutFileOpen(String datei)
    {
        FileOutputStream outFileStream = null;                       //der Ausgabestrom
        File outFile = null;                                         //die Datei

        outFile = new File(datei);

        //FileNotFoundException abfangen
        try
        {
            outFileStream = new FileOutputStream(outFile);
        }
        catch (Exception e)
        {
            System.out.println(e.getMessage());                      //wenn die Ausnahme aufgetreten ist...
            System.out.println("Der Zugriff auf die Datei wurde verweigert.");
        }
        return outFileStream;
    }

    /*
    *    Einlesen Zeichen für Zeichen und umwandeln in Großbuchstaben.
    *    Paramter: ein Datei-Eingabe- und ein Datei-Ausgabestrom
     */
    public void zeichenWeiseKopieren(FileInputStream in, FileOutputStream out)
    {
        char zeichen;
        int anzahl;
        try
        {
            anzahl = in.available();                          //Anzahl der Zeichen einlesen
            for(int i=0; i < anzahl; i++)                      //Einlesen der einzelnen Zeichen
            {
//                zeichen = java.lang.Character.toUpperCase((char)in.read());
                  //Umwandlung in Großbuchstaben
                  zeichen = Character.toUpperCase((char)in.read());
                  out.write(zeichen);
            }
        }
        catch (Exception e)
        {
            System.out.println(e.getMessage());
            System.out.println("Lese- oder Schreibefehler!");
        }
    }

    /*
    *   Einlesen der Datei in ein Char-Array, dann Umwandlung in Großbuchstaben und schreiben des
    *   Arrays in den Ausgabenstrom.
    *   Als Parameter werden die Dateieingabe- und Dateiausgabe erwartet.
    */
    public void charArrayKopieren(FileInputStream in, FileOutputStream out)
    {
        int anzahl;
        char[] tmp;
        InputStreamReader inputLesen;
        OutputStreamWriter outputSchreiben;
        BufferedReader buffIn;                                        //gepufferte Eingabe
        BufferedWriter buffOut;                                     //gepufferte Ausgabe

        try
        {
            anzahl = in.available();
            tmp = new char[anzahl];                                  //Einlesen der Zeichenanzahl
            inputLesen = new InputStreamReader(in);                  //Öffnen des Eingabestroms
            outputSchreiben = new OutputStreamWriter(out);           //Öffnen des Ausgabestroms
            buffIn = new BufferedReader(inputLesen,tmp.length);      //Öffnen des gepufferten Eingabestroms
            buffOut = new BufferedWriter(outputSchreiben, tmp.length);       //Öffnen des gepufferten Ausgabestroms
            //Feld gepuffert einlesen
            buffIn.read(tmp);
            //Zeichenweises Umwandeln in Grossbuchstaben
            for(int i = 0; i < anzahl; i++)
            {
                tmp[i] = Character.toUpperCase(tmp[i]);
            }
            buffOut.write(tmp);                                      //Schreiben in das Feld
            buffIn.close();                                          //Schließen des Eingabestroms
            inputLesen.close();
            buffOut.close();                                         //Schließen des Ausgabenstroms
            outputSchreiben.close();
        }
        catch (Exception e)
        {
            System.out.println(e.getMessage());
            System.out.println("Lese- oder Schreibefehler!");
        }
    }

    /**
     *   Zeichenweises Einlesen, dann Umwandlung in Großbuchstaben und Schreiben der Daten in einen gepufferten
     *   Ausgabestrom.
     *   Parameter sind DateiEin- und Ausgabestrom.
     */
    public void gepuffertKopieren(FileInputStream in,FileOutputStream out)
    {
        char zeichen;
        int anzahl;
        OutputStreamWriter ausgabeSchreiben;
        BufferedWriter buffOut;
        ausgabeSchreiben = new OutputStreamWriter(out);                            //Ausgabestrom öffnen
        buffOut = new BufferedWriter(ausgabeSchreiben);                            //gepufferten Ausgabestrom öffnen

        try
        {
            anzahl = in.available();
            //Zeichenweise einlesen, umwandeln und schreiben
            for(int i=0; i < anzahl; i++)
            {
                zeichen = Character.toUpperCase((char)in.read());
                buffOut.write(zeichen);
            }
            buffOut.close();
            ausgabeSchreiben.close();
        }
        catch (Exception e)
        {
            System.out.println(e.getMessage());
            System.out.println("Lese- oder Schreibefehler!");
        }
    }

    /**
     *    Öffnen der Eingabe- und Ausgabendatei übergeben als Argument und ruft die ausgewählte
     *    Kopierroutine auf. Dateinamen werden als String übergeben, der Kopiermodus als Zahl (int Wert).
     */
    public IOMainApp(String inFile, String outFile, int modus)
    {
        FileInputStream rein;
        FileOutputStream raus;

        rein = txtInFileOpen(inFile);                         //Eingabedatei
        raus = txtOutFileOpen(outFile);                       //Ausgabedatei

        switch(modus)
        {
            case 1:
            {
                System.out.println("Zeichenweise kopiert("+inFile+","+outFile+");");
                zeichenWeiseKopieren(rein,raus);
                break;
            }
            case 2:
            {
                System.out.println("Als Char-Array kopiert("+inFile+","+outFile+");");
                charArrayKopieren(rein,raus);
                break;
            }
            case 3:
            {
                System.out.println("Gepuffert kopiert("+inFile+","+outFile+");");
                gepuffertKopieren(rein,raus);
                break;
            }
            default:
            {
                System.out.println("Ungültige Auswahl! Bitte wählen Sie zwischen 1 - 3!");
                break;
            }
        }
        try
        {
            rein.close();
            raus.close();
        }
        catch (Exception e)
        {
            System.out.println(e.getMessage());
            System.out.println("Schreibfehler!");
        }
    }

    //Konstruktor
    public IOMainApp()
    {

    }

    /*
    *        Test
     */
    public static void main(String[] args)
    {
        //Test, ob Auswahl des Modus bei der Eingabe stimmt
        if(args.length < 3)
        {
            System.out.println("Aufgabe 1 EingabeDatei.txt AUSGABEDATEI.TXT modus");
            System.out.println("Modus = 1: Zeichenweise");
            System.out.println("Modus = 2: gepufferter In- und Output");
            System.out.println("Modus = 3: nur Ausgabe gepuffert");
        }
        else
        {
            System.out.println(Aufgabe1);
            System.out.println("Eingabedatei..: " +args[0]);
            System.out.println("Ausgabedatei..: " +args[1]);
            IOMainApp test = new IOMainApp(args[0],args[1],Integer.parseInt(args[2]));
        }
    }

}