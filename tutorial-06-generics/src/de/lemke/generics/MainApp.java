package de.lemke.generics;

public class MainApp
{
 public static void main(String [] args)
   {
     //wir erzeugen eine Box
     Box b = new Box();
     b.add(new String("Hallo"));
     System.out.format("Der Inhalt der Box ist: %s %n", b.getContent());

     Box b2 = new Box();
     b2.add(new Car());
     System.out.format("Der Inhalt der Box ist: %s %n", b2.getContent());

     //'unnötiger' type cast auf Car, weil es ja eigentlich ein Car-Objekt ist
     ((Car)b2.getContent()).move(100);     //type cast Car auf b2.getContent()

     //besserer Weg mit BoxGen
     BoxGen<Car> bg = new BoxGen<Car>();

     bg.add(new Car());
     bg.getContent().move(100);

     BoxGen<Book> regal = new BoxGen<Book>();
     regal.add(new Book());
     regal.getContent().lesen();

//     regal.getContent().move(100);          // Fehler!

     Fuhrpark<LKW> fp = new Fuhrpark<LKW>();
     fp.add(new LKW(40));
     fp.ausprobieren();


     Fuhrpark<Car> taxi = new Fuhrpark<Car>();
     taxi.add(new Car());
     taxi.ausprobieren();

     Pair<Integer, String> p1 = new Pair<Integer, String>(1,"Apfel");
     Pair<Integer, String> p2 = new Pair<Integer, String>(2,"Birne");

     //hier würde der Compiler das p3 bei einem Vergleich mit p1 oder p2 anmeckern, da p3 anders aufgebaut ist <String, Integer>
     Pair<String, Integer> p3 = new Pair<String, Integer>("Welt", 10);

     if(Tool.<Integer, String>vergleiche(p1,p2))
        System.out.println("...sind gleich.");
     else
       System.out.println("...sind ungleich.");

    //seit Java 7 geht auch dies: <Integer,Strings> muss nicht wiederholt werden und wird nur durch <> dargestellt
    Pair<Integer, String> p4 = new Pair<>(4,"Banane");

    if (Tool.vergleiche(p4,p4))
      System.out.println("...sind gleich");
   }
}
