package de.lemke.generics;

public class Car implements Vehicle
{
  private int mileage = 0;

  @Override
  public void move(int km)
     {
       this.mileage += km;
     }

  @Override
  public String toString()
    {
      return String.format("Ich bin ein %s und habe %d km auf dem Tacho.", this.getClass().getSimpleName(),this.mileage);
    }
}
