package de.lemke.generics;

public class LKW extends AVehicle
{
  private int capacity;

  public LKW(int t)
    {
      this.capacity = t;              //LKW-Zuladung in Tonnen
    }

  @Override
  public String toString()
   {
     return String.format("%s mit einer maximalen Zuladung von %d t.", super.toString(), this.capacity);
   }
}
