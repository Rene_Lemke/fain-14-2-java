package de.lemke.generics;

public abstract class AVehicle implements Vehicle
{
  private int mileage = 0;

  @Override
  public String toString()
    {
      return String.format("Ich bin ein Objekt der Klasse %s und habe %d auf dem Tacho", this.getClass().getSimpleName(), this.mileage);
    }

  @Override
  public void move(int km)
    {
      this.mileage += km;
    }
}
