package de.lemke.generics;

public class BoxGen<T>     // <T> = Typparameter
{
  private T content;

  public void add(T obj)
     {
       this.content = obj;
     }

  public T getContent()
    {
      return this.content;
    }
}
