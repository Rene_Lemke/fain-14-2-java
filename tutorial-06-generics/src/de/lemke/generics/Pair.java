package de.lemke.generics;

public class Pair<K,V>                  // K = Key, V = Value > Wertepaar
{
 private K key;
 private V value;

 public Pair(K key, V value)
   {
    this.key = key;
    this.value = value;
   }

 public K getKey()
   {
    return this.key;
   }

  public V getValue()
   {
    return this.value;
   }
}
