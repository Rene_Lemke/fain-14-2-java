package de.lemke.generics;

public interface Vehicle       //Interface
{
  public void move(int km);
}
