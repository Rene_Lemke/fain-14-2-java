package de.lemke.generics;

public class Fuhrpark<T extends Vehicle>
{
 private T slot;

 public Fuhrpark()
   {
   }

 public void add(T obj)                  //fügt Objekte dem Furhpark hinzu solange frei
   {
     this.slot = obj;
   }

 public void ausprobieren()
   {
     slot.move(1);            // funktioniert nur deshalb, weil durch den upper bound "extends Vehicle" Objekte der Klassenhierarchie
                          // für die generische Klasse verwendet werden müssen
   }
}
