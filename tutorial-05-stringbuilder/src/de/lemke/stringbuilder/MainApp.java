package de.lemke.stringbuilder;

public class MainApp
{
  public static void main(String [] args)
     {
      StringBuilder sbA = new StringBuilder();
      StringBuilder sbB = new StringBuilder("Hallo");
      StringBuilder sbC = new StringBuilder(100);
      StringBuilder palindrom = new StringBuilder("Test");

      System.out.format("sbA: \"%s\" %d%n", sbA, sbA.capacity());
      System.out.format("sbB: \"%s\" %d%n", sbB, sbB.capacity());
      System.out.format("sbC: \"%s\" %d%n", sbC, sbC.capacity());

      System.out.format("%n---Anhängen---%n");
      //Anhängen boolescher Werte
      sbA.append(true);
      System.out.format("sbA: \"%s\" %d%n", sbA, sbA.capacity());

      //Anhängen von Strings
      sbB.append(" Welt!");
      System.out.format("sbB: \"%s\" %d%n", sbB, sbB.capacity());

      //Anhängen anderer StringBuilder
      sbC.append(sbB);
      System.out.format("sbC: \"%s\" %d%n", sbC, sbC.capacity());

      System.out.format("%n---Einfügen---%n");
      //Insert, boolescher Werte
      sbA.insert(2, false);
      System.out.format("sbA: \"%s\" %d%n", sbA, sbA.capacity());

      //Insert String
      sbB.insert(5, " schöne");
      System.out.format("sbB: \"%s\" %d%n", sbB, sbB.capacity());

      //Insert StringBuilder
      sbC.insert(0,sbB);
      System.out.format("sbC: \"%s\" %d%n", sbC, sbC.capacity());

      System.out.format("%n---Ersetzen---%n");

      //Ersetzen String
      sbB.replace(6, 12, "friedliche");
      System.out.format("sbB: \"%s\" %d%n", sbB, sbB.capacity());

      //Palindrom eines Strings
      System.out.format("%s -> %s%n", palindrom, palindrom.reverse());
     }
}
