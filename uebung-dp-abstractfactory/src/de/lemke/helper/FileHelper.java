package de.lemke.helper;

import java.io.*;
import java.nio.charset.*;
import java.nio.file.*;

public class FileHelper
{
  private static BufferedWriter writer = null;

  public static void write(String content) throws IOException
    {
      Path file = Paths.get("outfile.txt");
      BufferedWriter writer = null;

      try
       {
         writer = Files.newBufferedWriter(file, StandardCharsets.UTF_8, StandardOpenOption.CREATE, StandardOpenOption.APPEND);
         writer.write(content,0,content.length());
       }
      catch(IOException e)
       {
         System.out.println(e);
       }
      finally
       {
         if(writer != null)
          {
            writer.close();
          }
       }
    }
}
