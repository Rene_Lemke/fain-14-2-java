package de.lemke.tables;

import de.lemke.helper.*;
import java.io.IOException;

public class FileRow extends Row
{
  @Override
  public void display()
   {
     //für alle Zellen der Zeile
     for(Cell cell: this.cells)
      {
        cell.display();                     //wird die Text-Ausgabe angeschubst
      }

    try
     {
       FileHelper.write(String.format("%n%s%n","--------------------------------"));
     }
    catch(IOException e)
     {
       System.err.println(e);
     }
   }
}
