package de.lemke.tables;

import de.lemke.helper.*;
import java.io.IOException;

public class FileTable extends Table
{
  @Override
  public void display()
   {
     try
      {
        FileHelper.write(String.format("%s%n", "--- Tabelle --------------------"));
      }
     catch(IOException e)
      {
        System.err.println(e);
      }

    //für jede Zeile der Tabelle
    for(Row row: this.rows)
     {
       row.display();              //wird die Text-Ausgabe angeschubst
     }
   }
}
