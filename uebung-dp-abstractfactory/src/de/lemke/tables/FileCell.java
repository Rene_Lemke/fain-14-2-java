package de.lemke.tables;

import de.lemke.helper.*;
import java.io.IOException;

public class FileCell extends Cell
{
  public FileCell(String c)
   {
     super(c);
   }

  @Override
  public void display()
   {
     try
      {
        FileHelper.write(String.format("%8s",this.content));
      }
     catch(IOException e)
      {
        System.err.println(e);
      }
   }
}
