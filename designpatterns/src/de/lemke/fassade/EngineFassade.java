package de.lemke.fassade;

import de.lemke.engine.*;

public class EngineFassade
{
  public static void prepareForAssembly(Engine engine)
    {
      engine.checkOil();
      engine.testRun();
      engine.getDocuments();
      engine.cleaning();
    }
}
