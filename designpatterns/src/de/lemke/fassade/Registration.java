package de.lemke.fassade;

import de.lemke.vehicle.*;

//für das Fassade-Pattern mal noch ein weiterer Mitspieler auf Seite des Subsystems
public class Registration
{
  private Vehicle vehicle;

  //Konstruktor
  public Registration(Vehicle vehicle)
    {
      this.vehicle = vehicle;
    }

  //Nummernschilder
  public void allocateLicensePlate()
    {
      //Code, um die Nummernschilder an das Fahrzeug zu schrauben
      System.out.println("Kennzeichen erworben und angeschraubt.");
    }

  //Fahrzeugpapiere ausstellen
  public void getVehicleDocuments()
    {
      System.out.println("Fahrzeugpapiere erstellt.");
    }
}
