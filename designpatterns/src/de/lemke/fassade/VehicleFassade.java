package de.lemke.fassade;

import de.lemke.vehicle.*;

public class VehicleFassade
{
  public void prepareForSale(Vehicle vehicle)
    {
      //den Papierkram erledigen
      Registration reg = new Registration(vehicle);

      reg.allocateLicensePlate();
      reg.getVehicleDocuments();

      //und nun das Fahrzeug selbst vorbereiten
      vehicle.testDrive();
      vehicle.cleanInterior();
      vehicle.cleanExterior();
      vehicle.polishWindows();
    }
}
