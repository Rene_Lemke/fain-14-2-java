package de.lemke.adapter;

import de.lemke.engine.Engine;

public class SemitrailerTruck extends Truck
{
  public SemitrailerTruck()
    {
      super();
    }

  public SemitrailerTruck(int p, Engine e)
    {
      super(p, e);
    }
}
