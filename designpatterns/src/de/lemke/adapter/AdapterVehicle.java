package de.lemke.adapter;

import de.lemke.vehicle.AbstractVehicle;

public class AdapterVehicle extends AbstractVehicle
{
  /**
  *    Der Konstruktor übernimmt die relevanten Eigenschaftswerte aus dem Truck und ruft damit
  *    den Konstruktor der Klasse AbstractVehicle auf
  **/
  public AdapterVehicle(Truck t)
    {
      super(t.getTruckEngine());
    }

  // im Zusammenspiel mit dem Dekorierer-Pattern haben wir die getPrice()-Methode zu implementieren
  public int getPrice()
    {
      return 0;
    }
}
