package de.lemke.adapter;

import de.lemke.engine.*;

public class AdapterEngine extends AbstractEngine
{
  //durch Ableitung von AbstractEngine kann unser Adapter automatisch getSize(), isTurbo() und toString()

  //der Konstruktor bekommt ein Objekt der zu adaptierenden Klasse übergeben
  public AdapterEngine(GreenEngine ge)
    {
      //wir rufen den Konstruktor der Basisklasse auf und übergeben den Hubraum und die Turboeigenschaft
      super(ge.getEngineSize(), false);
    }
}
