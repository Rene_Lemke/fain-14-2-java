package de.lemke.adapter;

import de.lemke.component.Window;
import de.lemke.engine.Engine;
import de.lemke.vehicle.AbstractVehicle;
import de.lemke.vehicle.Vehicle;

public class AdapterVehicleAlternative extends AbstractVehicle
{
  private Truck truck;

  public AdapterVehicleAlternative(Truck truck)
    {
      //der Aufruf des Konstruktors der Oberklasse ist laut Konstruktorregel #1 rein formal erforderlich
      super(null);

      this.truck = truck;
    }

      //Mapping der Methoden der AbstractVehicle-Klasse auf die Methoden des Trucks
      @Override
      public Engine getEngine()
       {
         return this.truck.getTruckEngine();
       }

      //Trucks haben keine Farbe
      @Override
      public Vehicle.Colour getColour()
        {
          return Colour.UNPAINTED;
        }

      //...und lassen sich auch nicht lackieren
      @Override
      public void paint(Vehicle.Colour c)
        {
          //tut nichts, weil ein Truck nicht lackiert werden kann
        }

      //Fenster werden auch nicht gebraucht. Wir haben so einen coolen Mad-Max-Jenseits-der-Donnerkuppel-Truck ...
      @Override
      public void addWindow(Window w)
        {
          //tut nichts, weil man keine Fenster einbauen kann
        }

      @Override
      public String toString()
        {
          return String.format("%s (%s, %s)", this.truck.getClass().getSimpleName(),this.getEngine(), this.getColour());
        }

      @Override
      public int getPrice()
        {
          return 0;
        }
}
