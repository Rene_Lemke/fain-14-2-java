package de.lemke.adapter;

public class GreenEngine
{
  //Hubraum, Turbo gibt es hier allerdings nicht
  private int size;

  //Konstruktor
  public GreenEngine(int s)
   {
     this.size = s;
   }

  //liefert den Hubraum
  public int getEngineSize()
   {
     return this.size;
   }
}
