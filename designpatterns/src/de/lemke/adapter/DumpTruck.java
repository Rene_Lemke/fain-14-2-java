package de.lemke.adapter;

import de.lemke.engine.Engine;

public class DumpTruck extends Truck
{
  public DumpTruck()
   {
     super();
   }

  public DumpTruck(int p, Engine e)
   {
     super(p,e);
   }
}
