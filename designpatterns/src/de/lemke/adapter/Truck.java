package de.lemke.adapter;

import de.lemke.builder.VehicleBuilder;
import de.lemke.engine.Engine;
import de.lemke.engine.StandardEngine;

public abstract class Truck
{
  private int payLoad;
  private Engine engine;

  public Truck()
   {
     this.payLoad = 0;
     this.engine = new StandardEngine(3000);
   }

  public Truck(int p, Engine e)
   {
     this.payLoad = p;
     this.engine = e;
   }

  public int getPayload()
   {
     return this.payLoad;
   }

  public Engine getTruckEngine()
   {
     return this.engine;
   }

  @Override
  public String toString()
   {
     return String.format("%s (%s)", this.getClass().getSimpleName(), this.getTruckEngine());
   }
}
