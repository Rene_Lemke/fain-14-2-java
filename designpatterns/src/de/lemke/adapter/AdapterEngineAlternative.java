package de.lemke.adapter;

import de.lemke.engine.*;

public class AdapterEngineAlternative extends AbstractEngine
{
  //wir schaffen die Möglichkeit, daß zu adaptierende Objekt zu referenzieren
  private GreenEngine greenEngine;

  //Konstruktor der alternativen Adapterklasse
  public AdapterEngineAlternative(GreenEngine ge)
    {
      //wir rufen den parameterbehafteten Konstruktor der Basisklasse auf (obwohl es eigentlich hier keinen Sinn macht)
      super(0, false);

      //wir speichern die Referenz auf das zu adaptierende Objekt
      this.greenEngine = ge;
    }

  @Override
  public int getSize()
   {
     //wir mappen getSize() auf getEngineSize()
     return this.greenEngine.getEngineSize();
   }

  @Override
  public boolean isTurbo()
   {
     //GreenEngine hat keine Aufladung
     return false;
   }

  @Override
  public String toString()
  {
    return String.format("%s (%d)", this.greenEngine.getClass().getSimpleName(), this.getSize());
  }
}
