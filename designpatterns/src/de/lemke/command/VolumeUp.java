package de.lemke.command;

public class VolumeUp extends AbstractCommand
{
    private Radio radio;

    // Konstruktor
    public VolumeUp(Radio r)
    {
        this.radio = r;
    }

    @Override
    public void execute()
    {
        //Code, um den Zugriff auf die Lautstärkeregelung zu protokollieren
        log(LogMessages.RADIO.VOL_UP);

        // wir delegieren den Aufruf von execute() an die Methode volumeUp() des Radios
        radio.volumeUp();
    }

    @Override
    public void undo()
    {
        log(LogMessages.RADIO.VOL_DOWN);
        //wir nehmen die Erhöhung der Lautstärke zurück
        radio.volumeDown();
    }
}