package de.lemke.command;

import java.util.Stack;

public class SpeechControl
{
    // die Sprachsteuerung soll UP und DOWN als Befehle erkennen
    private Command up;
    private Command down;
    private Command on;
    private Command off;

    //wir speichern alle Befehle in einem Verlauf (Stack)
    private Stack<Command> history;

    // setter based injection
    public void setCommands(Command u, Command d)
    {
        // was angesteuert wird hängt von den konkreten Command-Objekten ab
        this.up = u;
        this.down = d;

        //erzeuge eine neue History
        history = new Stack<Command>();
    }

    public void setCommands(Command u, Command d, Command on, Command off)
    {
        //Aufruf der Original setCommands-Methode
        setCommands(u,d);

        this.on = on;
        this.off = off;
    }

    // die Methoden der Spracherkennung
    public void hearUp()
    {
        // Aufruf von execute() für das injizierte Up Command-Objekt
        up.execute();

        //füge das up-Objekt in den Kellerspeicher ein
        history.push(up);
        //Protocol.addCommand(up);
    }

    public void hearDown()
    {
        // Aufruf von execute() für das injizierte Down Command-Objekt
        down.execute();

        //füge das down-Objekt in den Kellerspeicher ein
        history.push(down);
        //Protocol.addCommand(down);
    }

    public void hearUndo()
    {
        //nachschauen in einem Kellerspeicher (Stack), was wir zuletzt gemacht haben
        //für dieses Objekt rufen wir undo() auf und entnehmen das Objekt aus dem Stack
        if(!history.empty())
        {
            (history.pop()).undo();
        }
        else
        {
            System.out.println("Der Verlauf ist leer.");
        }
    }

    public void hearOn()
    {
        //nur was sich einschalten lässt
        if(on == null)
        {
            return;
        }

        //wird auch eingeschaltet
        on.execute();
        history.push(on);
    }

    public void hearOff()
    {
        //nur was sich ausschalten lässt
        if(off == null)
        {
          return;
        }

        //lässt sich auch ausschalten
        off.execute();
        history.push(off);
    }
}