package de.lemke.command;

public class RadioOff extends AbstractCommand
{
    private Radio radio;

    // Konstruktor
    public RadioOff(Radio r)
    {
        this.radio = r;
    }

    @Override
    public void execute()
    {
        log(LogMessages.RADIO.POWER_OFF);
        //das Radio wird ausgestellt
        radio.turnOff();
    }

    @Override
    public void undo()
    {
        log(LogMessages.RADIO.POWER_ON);
        //Rücknahme des letzten Befehls: das Radio wird wieder angestellt
        radio.turnOn();
    }
}