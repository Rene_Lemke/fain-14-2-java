package de.lemke.command;

public class WindowUp extends AbstractCommand
{
    private Window window;

    // Konstruktor
    public WindowUp(Window w)
    {
        this.window = w;
    }

    @Override
    public void execute()
    {
        log(LogMessages.WINDOW.WIN_UP);
        window.closeWindow();
    }

    @Override
    public void undo()
    {
        log(LogMessages.WINDOW.WIN_DOWN);
        //wir machen das Fenster wieder auf
        window.openWindow();
    }
}