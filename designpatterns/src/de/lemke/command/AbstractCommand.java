package de.lemke.command;

import java.io.IOException;
import java.util.logging.*;

public abstract class AbstractCommand implements Command
{
    //definieren eines Loggers
    private static final Logger log = Logger.getLogger("CommandPattern");

    //der statische Konstruktor (wird nur ein einziges Mal beim Laden der Klasse ausgeführt)
    static
     {
         try
          {
              //entfernt alle Handler (inkl. des console-Handlers vom Logger-Objekt)
              LogManager.getLogManager().reset();

              //richtet einen FileHandler ein
              FileHandler logfile = new FileHandler("logfile");

              //...formattiert Ausgaben über diesen FileHandler als einfachen Text
              logfile.setFormatter(new SimpleFormatter());

              //...registriert diesen FileHandler beim Logger
              log.addHandler(logfile);
          }
         catch(IOException e)
          {
              e.printStackTrace();
          }
     }

    //Protokolliert die Meldungen
    public void log(LogMessages.LogMessage msg)
     {
       //die Logmessage zusammenbasteln
         String message = String.format("[%s]: %s", this.getClass().getSimpleName(), msg.getMessage());

       //...und loggen
       log.log(Level.INFO, message);
     }
}
