package de.lemke.command;


public final class LogMessages
{
    //ein gemeinsamer Obertyp für die Enumerationen RADIO und WINDOW
    public interface LogMessage
     {
        public String getMessage();
     }

    public static enum RADIO implements LogMessage
    {
        VOL_UP("Lautstärke erhöhen."),
        VOL_DOWN("Lautstärke verringern."),
        POWER_ON("Eingeschaltet."),
        POWER_OFF("Ausgeschaltet.");

        private String message;

        RADIO(String msg)
         {
             message = msg;
         }

        @Override
        public String getMessage()
         {
             return message;
         }
    }

    public static enum WINDOW implements LogMessage
     {
       WIN_UP("Fenster ist offen."),
       WIN_DOWN("Fenster ist geschlossen.");

       private String message;

       WINDOW(String msg)
        {
          message = msg;
        }

       @Override
       public String getMessage()
        {
            return message;
        }
     }
}
