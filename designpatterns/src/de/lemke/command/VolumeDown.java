package de.lemke.command;

public class VolumeDown extends AbstractCommand
{
    private Radio radio;

    // Konstruktor
    public VolumeDown(Radio r)
    {
        this.radio = r;
    }

    @Override
    public void execute()
    {
        log(LogMessages.RADIO.VOL_DOWN);
        radio.volumeDown();
    }

    @Override
    public void undo()
    {
        log(LogMessages.RADIO.VOL_UP);
        //wir nehmen die Verringerung der Lautstärke zurück
        radio.volumeUp();
    }
}