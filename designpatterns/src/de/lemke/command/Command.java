package de.lemke.command;

public interface Command
{
    public void execute();

    public void undo();
}