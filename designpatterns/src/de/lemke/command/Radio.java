package de.lemke.command;

public class Radio
{
    public static final int MIN_VOLUME = 0;
    public static final int MAX_VOLUME = 10;
    public static final int DEFAULT_VOLUME = 5;

    private boolean state;                           // on = true; off = false
    private int volume;                              // die aktuelle Lautstärke

    //Konstruktor
    public Radio()
    {
        state = false;
        volume = DEFAULT_VOLUME;
    }

    public boolean isOn()
    {
        return state == true;
    }

    public int getVolume()
    {
        return volume;
    }

    public void turnOn()
    {
        state = true;
        // volume = DEFAULT_VOLUME;
        System.out.println("Das Radio ist nun eingeschaltet. Die Lautstärke ist: " + getVolume());
    }

    public void turnOff()
    {
        state = false;
        System.out.println("Das Radio ist nun ausgeschaltet.");
    }

    // die folgenden Methoden werden dann durch Command-Objekte angesteuert
    public void volumeUp()
    {
        if(isOn())
        {
            if(getVolume() < MAX_VOLUME)
            {
                volume++;
                System.out.println("Die Lautstärke wird erhöht auf: " + getVolume());
            }
        }
    }

    public void volumeDown()
    {
        if(isOn())
        {
            if(getVolume() > MIN_VOLUME)
            {
                volume--;
                System.out.println("Die Lautstärke wird verringert auf: " + getVolume());
            }
        }
    }
}