package de.lemke.command;

public class WindowDown extends AbstractCommand
{
    private Window window;

    // Konstruktor
    public WindowDown(Window w)
    {
        this.window = w;
    }

    @Override
    public void execute()
    {
        log(LogMessages.WINDOW.WIN_DOWN);
        window.openWindow();
    }

    @Override
    public void undo()
    {
        log(LogMessages.WINDOW.WIN_UP);
        //wir machen das Fenster wieder zu
        window.closeWindow();
    }
}