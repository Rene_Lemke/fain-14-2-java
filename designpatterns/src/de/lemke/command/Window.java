package de.lemke.command;

public class Window
{
    private boolean open;

    public Window()
    {
        open=false;
        System.out.println("Das Fenster ist geschlossen.");
    }

    public boolean isOpen()
    {
        return open;
    }

    // die folgenden Methoden werden durch Command-Objekte angesteuert
    public void openWindow()
    {
        if(!isOpen())
        {
            open = true;
            System.out.println("Das Fenster wurde geöffnet.");
        }
    }

    public void closeWindow()
    {
        if(isOpen())
        {
            open = false;
            System.out.println("Das Fenster wurde geschlossen.");
        }
    }
}