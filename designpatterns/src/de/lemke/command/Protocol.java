package de.lemke.command;

import java.util.Stack;

public abstract class Protocol
{
    public static Stack<Command> protocol = new Stack<Command>();

    public static void addCommand(Command command)
    {
        protocol.push(command);
    }

    public static void getLastCommand()
    {
        if(!protocol.isEmpty())
        {
            (protocol.pop()).undo();
        }
        else
        {
            System.out.println("Das Protokoll ist Leer.");
        }
    }
}
