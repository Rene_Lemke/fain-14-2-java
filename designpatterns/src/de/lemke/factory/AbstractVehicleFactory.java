package de.lemke.factory;

import de.lemke.vehicle.*;
import de.lemke.engine.*;

public abstract class AbstractVehicleFactory
{
  //der Kunde entscheidet sich für einen Fahrstil
  public enum DrivingStyle {ECO, MID, POWER};

  //diese Methode benutzt der Kunde, um sein Fahrzeug bauen zu lassen
  public Vehicle build(DrivingStyle style, Vehicle.Colour colour)
    {
      //hier erfolgt der Aufruf der Fabrikmethode
      Vehicle v = selectVehicle(style);

      //das Fahrzeug wird lackiert
      v.paint(colour);

      //das Fahrzeug wird an den Kunden ausgeliefert
      return v;
    }

  //das hier ist die eigentliche Fabrikmethode, die durch die abgeleiteten Fabrikklassen noch implementiert werden muss
  protected abstract Vehicle selectVehicle(DrivingStyle style);
}
