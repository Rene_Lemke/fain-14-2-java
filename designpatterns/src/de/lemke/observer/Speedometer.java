package de.lemke.observer;

import java.util.Observable;

public class Speedometer extends Observable
{
    private int currentSpeed;

    //Konstruktor
    public Speedometer()
    {
        currentSpeed = 0;
    }

    //legt eine neue Geschwindigkeit fest (Zustandsänderung)
    public void setCurrentSpeed(int speed)
    {
        currentSpeed = speed;

        //wir müssen nun auch die Beobachter informieren
        setChanged();                                          //definiert in Observable
        notifyObservers();
    }

    //liefert als Ergebnis die aktuelle Geschwindigkeit
    public int getCurrentSpeed()
    {
        return this.currentSpeed;
    }
}
