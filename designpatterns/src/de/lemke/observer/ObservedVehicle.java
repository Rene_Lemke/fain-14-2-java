package de.lemke.observer;

import de.lemke.engine.Engine;
import de.lemke.vehicle.AbstractVehicle;
import java.util.List;
import java.util.ArrayList;

public class ObservedVehicle extends AbstractVehicle
{
    List<Inspektor> beobachter;
    //Konstruktor
    public ObservedVehicle(Engine e)
    {
        //ruft den Konstruktor der Basisklasse auf
        super(e);

        //die Beobachterklasse instanziieren
        beobachter = new ArrayList<>();
    }

    //Methoden zum Verwalten der Beobachter
    public void addObserver(Inspektor o)
    {
        beobachter.add(o);

        //System.out.println(this.beobachter.toArray());
    }

    public void removeObserver(Inspektor o)
    {
        beobachter.remove(o);
    }

    //wird immer dann aufgerufen, wenn eine Methode den Zustand des beobachteten Vehicles geändert hat
    public void notifyMyObservers()
    {
        for(Inspektor o: beobachter)
        {
            o.inform(this);
        }
    }

    public int getMileage()
    {
        return mileage;
    }

    @Override
    public void move(int km)
    {
        super.move(km);
        this.notifyMyObservers();
    }

    @Override
    public int getPrice()
    {
        return 0;
    }
}
