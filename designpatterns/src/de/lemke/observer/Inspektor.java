package de.lemke.observer;

public interface Inspektor
{
    public void inform(ObservedVehicle v);
}
