package de.lemke.observer;

public class IntervallInspektor implements Inspektor
{
    private int naechsteInspektion = 10000;

    @Override
    public void inform(ObservedVehicle v)
    {
        if(v.getMileage() >= naechsteInspektion)
        {
            System.out.println("Die Intervallinspektion wird durchgeführt!");

            naechsteInspektion += 10000;
        }
    }
}
