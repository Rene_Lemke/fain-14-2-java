package de.lemke.observer;

public class ErstInspektor implements Inspektor
{
        private static final int KM = 5000;

        @Override
        public void inform(ObservedVehicle v)
         {
           if(v.getMileage() >= KM)
           {
               System.out.println("Erstinspektion ist fällig!");

               //nach der Inspektion wird dieser Beobachter nicht mehr gebraucht
               v.removeObserver(this);
           }
         }
}
