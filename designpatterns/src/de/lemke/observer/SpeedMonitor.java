package de.lemke.observer;

import java.util.Observable;
import java.util.Observer;

public class SpeedMonitor implements Observer
{
    //wir definieren den Schwellwert, ab dem Alarm ausgelöst werden soll
    public static final int SPEED_TO_ALERT = 70;

    //die Methode wird vom Beobachteten aufgerufen, der damit signalisiert: "es hat sich etwas bei mir geändert"
    //der Beobachtete übergibt als Parameter eine Referenz auf sich selbst, damit die Beobachter eine Möglichkeit haben,
    //den sie interessierenden Zustand abzurufen
    @Override
    public void update(Observable o, Object arg)
    {
        Speedometer speedo = (Speedometer) o;

        //wir holen uns vom Speedometer die aktuelle Geschwindigkeit
        if(speedo.getCurrentSpeed() > SPEED_TO_ALERT)
        {
            System.out.println("**ALARM** - Sie fahren zu schnell! " + speedo.getCurrentSpeed());
        }
        else
        {
            System.out.println("Alles im grünen Bereich: " + speedo.getCurrentSpeed());
        }
    }
}
