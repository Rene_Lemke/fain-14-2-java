package de.lemke.observer;


import java.util.Observer;
import java.util.Observable;

public class GearBox implements Observer
{
    //beim Beschleunigen sollen Schaltempfehlungen generiert werden
    @Override
    public void update(Observable o, Object arg)
    {
        Speedometer speedo = (Speedometer) o;

        if(speedo.getCurrentSpeed() <= 10)
        {
            System.out.println("Erster Gang.");
        }
        else if(speedo.getCurrentSpeed() <= 20)
        {
            System.out.println("Zweiter Gang.");
        }
        else if(speedo.getCurrentSpeed() <= 50)
        {
            System.out.println("Dritter Gang.");
        }
        else
        {
            System.out.println("Vierter Gang.");
        }
    }
}
