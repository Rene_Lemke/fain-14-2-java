package de.lemke.state;

import java.util.Calendar;

public class MinuteSetupState implements ClockSetupState
{
    //speichert die eingestellte Minute
    private int minute;

    //speichert das Context-Objekt
    private ClockSetup clockSetup;

    //Konstruktor
    public MinuteSetupState(ClockSetup cs)
    {
        this.clockSetup = cs;
        //Startwert ist die aktuelle Stunde
        this.minute = Calendar.getInstance().get(Calendar.MINUTE);
    }

    @Override
    public void previousValue()
    {
        if(this.minute > 0)
        {
            this.minute--;
        }
    }

    @Override
    public void nextValue()
    {
        if(this.minute < 59)
        {
            this.minute++;
        }
    }

    @Override
    public void selectValue()
    {
        System.out.println("Minute wurde gesetzt auf: " + this.minute);

        //Zustandswechsel signalisieren
        this.clockSetup.setState(clockSetup.getFinishedSetupState());
    }

    @Override
    public String getInstructions()
    {
        return "Bitte geben Sie die Minute an ...";
    }

    @Override
    public int getSelectedValue()
    {
        return this.minute;
    }
}
