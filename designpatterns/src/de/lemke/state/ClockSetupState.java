package de.lemke.state;

public interface ClockSetupState
{
    //falls jemand den Knopf nach links dreht
    public void previousValue();

    //falls jemand den Knopf nach rechts dreht
    public void nextValue();

    //falls jemand den Knopf drückt
    public void selectValue();

    //zum Anzeigen eines Hilfetextes
    public String getInstructions();

    //zum Abrufen des eingestellten Wertes
    public int getSelectedValue();
}
