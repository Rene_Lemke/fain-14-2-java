package de.lemke.state;

import java.util.Calendar;
import java.util.GregorianCalendar;

public class ClockSetup
{
  //Referenzen der Zustände
  private ClockSetupState yearState;
  private ClockSetupState monthState;
  private ClockSetupState dayState;
  private ClockSetupState hourState;
  private ClockSetupState minuteState;
  private ClockSetupState finishedState;

  //Welcher ist der aktuelle Zustand?
  private ClockSetupState currentState;

  //Konstruktor
  public ClockSetup()
  {
      //dem YearSetupState wird der Context injiziert
      this.yearState = new YearSetupState(this);

      //dem MonthSetupState wird der Context injiziert
      this.monthState = new MonthSetupState(this);

      //dem DaySetupState wird der Context injiziert
      this.dayState = new DaySetupState(this);

      //dem HourSetupState wird der Context injiziert
      this.hourState = new HourSetupState(this);

      //dem MinuteSetupState wird der Context injiziert
      this.minuteState = new MinuteSetupState(this);

      //dem FinishedSetupState wird der Context injiziert
      this.finishedState = new FinishedSetupState(this);

      //wir legen den Anfangszustand fest
      this.setState(this.yearState);
  }

  //Methode, für den Zustandswechsel
  public void setState(ClockSetupState state)
  {
      //speichert den aktuellen Zustand
      this.currentState = state;

      //...und gibt die Anweisungen für den nächsten Zustand
      System.out.println(this.currentState.getInstructions());
  }

  //liefert den Zustand zum Ändern des Jahres
  public ClockSetupState getYearSetupState()
  {
      return this.yearState;
  }

  //liefert den Zustand zum Ändern des Monats
  public ClockSetupState getMonthSetupState()
  {
      return this.monthState;
  }

  //liefert den Zustand zum Ändern des Tages
  public ClockSetupState getDaySetupState()
  {
      return this.dayState;
  }

  //liefert den Zustand zum Ändern der Stunde
  public ClockSetupState getHourSetupState()
  {
      return this.hourState;
  }

  //liefert den Zustand zum Ändern der Minute
  public ClockSetupState getMinuteSetupState()
  {
      return this.minuteState;
  }

  //liefert den finalen Zustand
  public ClockSetupState getFinishedSetupState()
  {
      return this.finishedState;
  }

  //liefert das Datum
  public Calendar getSelectedDate()
  {
      return new GregorianCalendar(
                    yearState.getSelectedValue(),
                    monthState.getSelectedValue(),
                    dayState.getSelectedValue(),
                    hourState.getSelectedValue(),
                    minuteState.getSelectedValue());
  }

  //die Bedienung der Uhr
  public void rotateButtonLeft()
  {
      //Wert verringern
      this.currentState.previousValue();
  }

  public void rotateButtonRight()
  {
      //Wert erhöhen
      this.currentState.nextValue();
  }

  public void pushButton()
  {
      //Eingabe bestätigen
      this.currentState.selectValue();
  }
}
