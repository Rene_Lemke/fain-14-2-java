package de.lemke.state;

import java.util.Calendar;

public class HourSetupState implements ClockSetupState
{
    //speichert die eingestellte Stunde
    private int hour;

    //speichert das Context-Objekt
    private ClockSetup clockSetup;

    //Konstruktor
    public HourSetupState(ClockSetup cs)
    {
        this.clockSetup = cs;
        //Startwert ist die aktuelle Stunde
        this.hour = Calendar.getInstance().get(Calendar.HOUR);
    }

    @Override
    public void previousValue()
    {
        if(this.hour > 0)
        {
            this.hour--;
        }
    }

    @Override
    public void nextValue()
    {
        if(this.hour < 23)
        {
            this.hour++;
        }
    }

    @Override
    public void selectValue()
    {
        System.out.println("Stunde wurde gesetzt auf: " + this.hour);

        //Zustandswechsel signalisieren
        //auf die Einstellung der Stunde folgt die Einstellung der Minute
        this.clockSetup.setState(clockSetup.getMinuteSetupState());
    }

    @Override
    public String getInstructions()
    {
        return "Bitte geben Sie die Stunde an ...";
    }

    @Override
    public int getSelectedValue()
    {
        return this.hour;
    }
}
