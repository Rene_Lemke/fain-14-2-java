package de.lemke.state;

import java.util.Calendar;

public class DaySetupState implements ClockSetupState
{
    //speichert den eingestellten Tag
    private int day;

    //speichert eine Referenz auf das Context-Objekt
    private ClockSetup clockSetup;

    //Konstruktor
    public DaySetupState(ClockSetup cs)
    {
        this.clockSetup = cs;
        //speichert den aktuellen Tag des Monats als Startwert
        this.day = Calendar.getInstance().get(Calendar.DAY_OF_MONTH);
    }


    @Override
    public void previousValue()
    {
        if(this.day > 1)
        {
            this.day--;
        }
    }

    @Override
    public void nextValue()
    {
     //prüfen, ob noch kleiner als der jeweils letzte Tag eines Monats inklusive Schaltjahren
     if(this.day < Calendar.getInstance().getActualMaximum(Calendar.DAY_OF_MONTH))
      {
          this.day++;
      }
    }

    @Override
    public void selectValue()
    {
      System.out.println("Tag wurde gesetzt auf: " + this.day);

      //Zustandswechsel signalisieren
      //nach dem Tag müssen die Stunden eingestellt werden
      this.clockSetup.setState(clockSetup.getHourSetupState());
    }

    @Override
    public String getInstructions()
    {
        return "Bitte geben Sie den Tag an ...";
    }

    @Override
    public int getSelectedValue()
    {
        return this.day;
    }
}
