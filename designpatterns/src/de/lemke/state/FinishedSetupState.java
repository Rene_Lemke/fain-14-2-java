package de.lemke.state;

import java.util.Calendar;

public class FinishedSetupState implements ClockSetupState
{
    //Referenz auf den Context
    private ClockSetup clockSetup;

    //Konstruktor
    public FinishedSetupState(ClockSetup cs)
    {
      this.clockSetup = cs;
    }

    @Override
    public void previousValue()
    {
        //es gibt nichts zu tun
    }

    @Override
    public void nextValue()
    {
      //es gibt nichts zu tun
    }

    @Override
    public void selectValue()
    {
        //das eingestellte Datum wird über den Context abgeholt
        Calendar selectedDate = clockSetup.getSelectedDate();
        System.out.println("Datum wurde gesetzt auf: " + selectedDate.getTime());
    }

    @Override
    public String getInstructions()
    {
        return "Den Knopf drücken, um daß gewählte Datum zu zeigen...";
    }

    @Override
    public int getSelectedValue()
    {
        throw new UnsupportedOperationException("Uhreinstellung ist beendet.");
    }
}
