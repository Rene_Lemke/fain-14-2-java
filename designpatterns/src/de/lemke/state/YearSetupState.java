package de.lemke.state;

import java.util.Calendar;

public class YearSetupState implements ClockSetupState
{
    //das eingestellte Jahr
    private int year;

    //eine Referenz auf das Context-Objekt, um den Zustandswechsel
    //zu signalisieren, damit der Zustandswechsel im Context erfasst werden kann
    private ClockSetup clockSetup;

    //Konstruktor
    //über den Konstruktor wird das Context-Objekt injiziert
    public YearSetupState(ClockSetup clocksetup)
    {
        this.clockSetup = clocksetup;
        //das aktuelle Jahr wird zum Startwert
        this.year = Calendar.getInstance().get(Calendar.YEAR);

    }

    @Override
    public void previousValue()
    {
      this.year--;
    }

    @Override
    public void nextValue()
    {
      this.year++;
    }

    @Override
    public void selectValue()
    {
      System.out.println("Jahr wurde gesetzt auf: " + this.year);

      //wir signalisieren den Zustandswechsel an das Context-Objekt
      //der Context stellt den Folgezustand ein: YEAR -> MONTH
      this.clockSetup.setState(clockSetup.getMonthSetupState());
    }

    @Override
    public String getInstructions()
    {
        return "Bitte geben Sie das Jahr ein ...";
    }

    @Override
    public int getSelectedValue()
    {
        return this.year;
    }
}
