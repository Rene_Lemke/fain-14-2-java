package de.lemke.state;

import java.util.Calendar;

public class MonthSetupState implements ClockSetupState
{
    //speichert den eingestellten Monat
    private int month;

    //speichert das Context-Objekt
    private ClockSetup clockSetup;

    //Konstruktor
    public MonthSetupState(ClockSetup cs)
    {
        this.clockSetup = cs;
        //Startwert ist der aktuelle Monat
        this.month = Calendar.getInstance().get(Calendar.MONTH);
    }

    @Override
    public void previousValue()
    {
      if(this.month > 0)
      {
          this.month--;
      }
    }

    @Override
    public void nextValue()
    {
        if(this.month < 11)                    //Kalendarrechnung in Java geht von 0 - 11 Monaten
        {
            this.month++;
        }
    }

    @Override
    public void selectValue()
    {
      System.out.println("Monat wurde gesetzt auf: " + this.month);

      //Zustandswechsel signalisieren
      //auf die Einstellung des Monats folgt die Einstellung des Tages
      this.clockSetup.setState(clockSetup.getDaySetupState());
    }

    @Override
    public String getInstructions()
    {
        return "Bitte geben Sie den Monat an ...";
    }

    @Override
    public int getSelectedValue()
    {
        return this.month;
    }
}
