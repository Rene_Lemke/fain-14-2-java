package de.lemke.composite;

public abstract class Item {

  // die Eigenschaften eines Items
  private String description;
  private int cost;

  // Konstruktor
  public Item(String s, int c) {

    this.description = s;
    this.cost = c;
  }

  // Zugriff auf die privaten Eigenschaften
  public String getDescription() {

    return this.description;
  }

  public int getCost() {

    return this.cost;
  }

  // abstrakte Methoden, die durch die Klassen Part und Component
  // individuell implementiert werden, weil sich ein Part ja nun
  // mal anders verhält als eine Komponente
  public abstract void addItem(Item item);
  public abstract void removeItem(Item item);
  public abstract Item[] getItems();

  public abstract void ausgabe();

  @Override
  public String toString() {

    return String.format("%s: (%3d €)", this.description, this.getCost());
  }

}
