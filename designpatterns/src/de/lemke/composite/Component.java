package de.lemke.composite;

import java.util.*;

public class Component extends Item {

  private List<Item> items;

  // Konstruktor
  public Component(String s) {

    super(s, 0);
    this.items = new ArrayList<Item>();
  }

  // wir überschreiben die abstrakten Methoden
  public void addItem(Item item) {

    this.items.add(item);
  }

  public void removeItem(Item item) {

    this.items.remove(item);
  }

  public Item[] getItems() {

    return this.items.toArray(new Item[this.items.size()]);
  }

  // wir müssen die Methode getCost() überschreiben, damit diese die Gesamtkosten der Struktur ermittelt
  public int getCost() {

    int total = 0;

    for(Item item: this.items) {

      total += item.getCost();  // ist Item ein Part, dann kommen die Part-Kosten zur Summe dazu
      // ist Item eine Component, dann werden die Kosten der Component ermittelt
    }

    return total;
  }

  private void indentation(int i) {

    while(i-->0) {

      System.out.print(" ");
    }
  }

  public void ausgabeHelper(Item i, int indent) {

    this.indentation(indent);
    System.out.println("┕" + i);

    for(Item item: i.getItems()) {

      if(item instanceof Component) {

        this.ausgabeHelper(item, indent+1);
      }
      else {

        this.indentation(indent+1);
        System.out.println("┕" + item);
      }
    }
  }

  public void ausgabe() {

    System.out.println(this);

    for(Item item: this.items) {

      if(item instanceof Component) {

        this.ausgabeHelper(item,1);
      }
      else {
        item.ausgabe();
      }
    }
  }
}
