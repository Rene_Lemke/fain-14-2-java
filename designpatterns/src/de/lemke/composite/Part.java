package de.lemke.composite;

public class Part extends Item {

  // Konstruktor
  public Part(String s, int c) {

    super(s, c);
  }

  // die pro-forma-Implementierung der abstrakten Methoden
  @Override
  public void addItem(Item item) {

    // hier tun wir nix, weil ein Part die kleinstmögliche Einheit ist
  }

  @Override
  public void removeItem(Item item) {

    // hier tun wir nix, weil ein Part die kleinstmögliche Einheit ist
  }

  @Override
  public Item[] getItems() {

    // hier tun wir nix, weil ein Part die kleinstmögliche Einheit ist
    return null;
  }

  @Override
  public void ausgabe() {

    System.out.println(this.toString());
  }
}
