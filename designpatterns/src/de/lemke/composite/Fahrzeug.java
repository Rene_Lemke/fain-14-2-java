package de.lemke.composite;

public class Fahrzeug implements Verleih
{
  private String name;

  //Konstruktor
  public Fahrzeug(String n)
    {
      this.name = n;
    }

  //die Implementierung von toConsole() für ein einzelnes Fahrzeug
  @Override
  public void toConsole()
    {
      System.out.println(this.getClass().getSimpleName() + ": " +this.name);
    }

  @Override
  public void toFile()
    {
      
    }
}
