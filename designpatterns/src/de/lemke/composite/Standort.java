package de.lemke.composite;

import java.util.*;

public class Standort implements Verleih
{
  private String name;

  //der Standort verwaltet die Fahrzeuge (weil Fahrzeuge Verleih implementieren, sind Fahrzeuge eine Art Verleih)
  private List<Verleih> liste;

  public Standort(String n)
    {
      this.name=n;
      this.liste = new ArrayList<Verleih>();
    }

  //Elemente zum Standort hinzufügen (Fahrzeuge oder Standorte)
  public void add(Verleih v)
    {
      this.liste.add(v);
    }

  //die Methode ist zugeschnitten darauf, Fahrzeuge oder Standorte auszugeben
  @Override
  public void toConsole()
    {
      System.out.println(this.getClass().getSimpleName() + ": " + this.name);

      for(Verleih v: this.liste)
        {
          v.toConsole();          //referenziert v einen Standort, dann führt v.toConsole() zu einer Rekursion
        }
    }

  @Override
  public void toFile()
    {
      
    }
}
