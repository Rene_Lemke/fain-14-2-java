package de.lemke.prototype;

import de.lemke.vehicle.*;
import de.lemke.engine.*;

public class VehicleManagerLazy
{
  private Vehicle saloon;
  private Vehicle coupe;
  private Vehicle sport;
  private Vehicle boxvan;
  private Vehicle pickup;

  //Methoden, um die Klone anzufertigen
  public Vehicle createSaloon()
    {
      if(this.saloon == null)
        {
          this.saloon = new Saloon(new StandardEngine(1300));
        }

      return (Vehicle)this.saloon.clone();                    //klonen und casten auf Vehicle, geliefert wird ein Saloon-Objekt
    }

  public Vehicle createCoupe()
    {
      if(this.coupe == null)
        {
          this.coupe = new Coupe(new StandardEngine(1800));
        }

      return (Vehicle) this.coupe.clone();
    }

  public Vehicle createSport()
    {
      if(this.sport == null)
        {
          this.sport = new Sport(new TurboEngine(2000));
        }

      return (Vehicle)this.sport.clone();
    }

  public Vehicle createBoxVan()
    {
      if(this.boxvan == null)
        {
          this.boxvan = new BoxVan(new StandardEngine(3000));
        }

      return (Vehicle) this.boxvan.clone();
    }

  public Vehicle createPickup()
    {
      if(this.pickup == null)
        {
          this.pickup = new Pickup(new TurboEngine(3000));
        }

      return (Vehicle)this.pickup.clone();
    }
}
