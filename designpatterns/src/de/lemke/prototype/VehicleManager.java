package de.lemke.prototype;

import de.lemke.vehicle.*;
import de.lemke.engine.*;

public class VehicleManager
{
  private Vehicle saloon;
  private Vehicle coupe;
  private Vehicle sport;
  private Vehicle boxvan;
  private Vehicle pickup;

  //der Konstruktor erzeugt die Prototypen
  public VehicleManager()
   {
     this.saloon = new Saloon(new StandardEngine(1300));
     this.coupe = new Coupe(new StandardEngine(1600));
     this.sport = new Sport(new TurboEngine(2000));

     this.boxvan = new BoxVan(new StandardEngine(2000));
     this.pickup = new Pickup(new TurboEngine(3000));
   }

  //Methoden, um die Klone anzufertigen
  public Vehicle createSaloon()
    {
      return (Vehicle)this.saloon.clone();         //wir klonen und casten auf Vehicle, geliefert wird ein Saloon-Objekt
    }

  public Vehicle createCoupe()
    {
      return (Vehicle)this.coupe.clone();
    }

  public Vehicle createSport()
    {
      return (Vehicle)this.sport.clone();
    }

  public Vehicle createBoxVan()
    {
      return (Vehicle)this.boxvan.clone();
    }

  public Vehicle createPickup()
    {
      return (Vehicle) this.pickup.clone();
    }
}
