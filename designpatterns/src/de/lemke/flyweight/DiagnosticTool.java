package de.lemke.flyweight;

public interface DiagnosticTool
{
  public void runDiagnosis(Object o);
}
