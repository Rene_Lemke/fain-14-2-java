package de.lemke.flyweight;

import de.lemke.engine.*;
import java.util.*;

public class EngineFlyweightFactory
{
  private Map<Integer, Engine> standardEnginePool;
  private Map<Integer, Engine> turboEnginePool;

  //Konstruktor
  public EngineFlyweightFactory()
    {
      this.standardEnginePool = new HashMap<Integer, Engine>();
      this.turboEnginePool = new HashMap<Integer, Engine>();
    }

  //die Engines unterscheiden sich hauptsächlich im Hubraum
  public Engine getStandardEngine(int size)
    {
      //nachschauen, ob es einen solchen Motor bereits gibt:
      Engine e = this.standardEnginePool.get(size);

      if(e == null)
        {
          //ggfs. eine neue Engine erzeugen
          e = new StandardEngine(size);

          //...und in der Map speichern
          this.standardEnginePool.put(size, e);
        }

      //die Engine wird ausgeliefert
      return e;
    }

   public Engine getTurboEngine(int size)
    {
      //nachschauen, ob es einen solchen Motor bereits gibt:
      Engine e = this.turboEnginePool.get(size);

      if(e == null)
        {
          //ggfs. eine neue Engine erzeugen
          e = new TurboEngine(size);

          //...und in der Map speichern
          this.turboEnginePool.put(size, e);
        }

      //die Engine wird ausgeliefert
      return e;
    }
}
