package de.lemke.decorator;

import de.lemke.vehicle.*;

public class AlloyWheeledVehicle extends AbstractVehicleOption
{
    // Konstruktor
    public AlloyWheeledVehicle(Vehicle vehicle)
      {
        super(vehicle);
      }

   // Alufelgen haben einen Aufpreis
   @Override
   public int getPrice()
     {
       return this.decoratedVehicle.getPrice() + 250;
     }

   @Override
   public int getDailyRate()
     {
       return this.decoratedVehicle.getDailyRate() + 3;    // Alufelgen kosten 3,- € mehr pro Tag
     }

   // hier folgen die optionsspezifischen Methoden ...
}
