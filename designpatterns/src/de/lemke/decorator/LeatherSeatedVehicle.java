package de.lemke.decorator;

import de.lemke.vehicle.*;

public class LeatherSeatedVehicle extends AbstractVehicleOption
{
    // Konstruktor
    public LeatherSeatedVehicle(Vehicle vehicle)
      {
        super(vehicle);
      }

    // ein Fahrzeug mit Ledersitzen kostet extra
    @Override
    public int getPrice()
      {
        return this.decoratedVehicle.getPrice() + 1200;
      }

    @Override
    public int getDailyRate()
      {
        return this.decoratedVehicle.getDailyRate();       // Ledersitze ohne Aufpreis
      }

   // hier folgen die optionsspezifischen Methoden ...
}
