package de.lemke.decorator;

import de.lemke.vehicle.*;

public class SpoileredVehicle extends AbstractVehicleOption
{
    // Konstruktor
    public SpoileredVehicle(Vehicle vehicle)
      {
        super(vehicle);
      }

    // Spoiler gegen Aufpreis
    @Override
    public int getPrice()
      {
        return this.decoratedVehicle.getPrice() + 600;
      }

    @Override
    public int getDailyRate()
      {
        return this.decoratedVehicle.getDailyRate() + 7;    // Spoiler kosten 7,- € extra
      }

    // hier folgen dann optionsspezifische Methoden ...
}
