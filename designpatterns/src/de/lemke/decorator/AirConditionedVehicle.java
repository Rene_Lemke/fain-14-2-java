package de.lemke.decorator;

import de.lemke.vehicle.*;

public class AirConditionedVehicle extends AbstractVehicleOption
{
  //Konstruktor
  public AirConditionedVehicle(Vehicle vehicle)
     {
       super(vehicle);
     }

   @Override
   public int getPrice()
     {
       //der Preis eines Fahrzeugs mit Klimaanlage ist um 600,- € teurer
       return this.decoratedVehicle.getPrice() + 600;
     }

   @Override
   public int getDailyRate()
     {
       return this.decoratedVehicle.getDailyRate() + 12;    //Klimaanlage kostet 12,- € mehr pro Tag
     }

   // hier folgen dann optionsspezifische Methoden ...
}
