package de.lemke.decorator;

import de.lemke.vehicle.*;

public class SatNavVehicle extends AbstractVehicleOption
{
    // Konstruktor
    public SatNavVehicle(Vehicle vehicle)
      {
        super(vehicle);
      }

   // Navi gegen Aufpreis
   @Override
   public int getPrice()
     {
       return this.decoratedVehicle.getPrice() + 300;
     }

   @Override
   public int getDailyRate()
     {
       return this.decoratedVehicle.getDailyRate() + 5;    // Navi kostet 5,- € mehr pro Tag
     }

   // hier folgen die optionsspezifischen Methoden ...
}
