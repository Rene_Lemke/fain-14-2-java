package de.lemke.decorator;

import de.lemke.vehicle.*;

public abstract class AbstractVehicleOption extends AbstractVehicle
{
     //hier wird eine Referenz auf ein (dekoriertes) Vehicle gespeichert
     protected Vehicle decoratedVehicle;

     //Konstruktor, über den das Vehicle injiziert wird
     public AbstractVehicleOption(Vehicle vehicle)
       {
         //Konstruktor der Basisklasse aufrufen
         super(vehicle.getEngine());

         this.decoratedVehicle = vehicle;
       }

    //liefert das dekorierte Vehicle zurück
    public Vehicle getDecoratedVehicle()
      {
        return this.decoratedVehicle;
      }

    //liefert das dekorierte Vehicle, aus dem der gewünschte Dekorierer entfernt wurde
    public Vehicle removeDecorator(Vehicle vehicle, String decoratorName)
      {
        //wenn wir im inneren Kern angekommen sind ...
        if(!(vehicle instanceof AbstractVehicleOption))
          {
            return vehicle;
          }

        Vehicle innerVehicle = ((AbstractVehicleOption)vehicle).getDecoratedVehicle();

        //falls das "innere" Vehicle das zu löschende dekorierte Vehicle ist ...
        if(innerVehicle.getClass().getSimpleName().equals(decoratorName))
          {
            //dann verlinken wir vom aktuellen auf das "übernächste"
            ((AbstractVehicleOption)vehicle).decoratedVehicle = ((AbstractVehicleOption)innerVehicle).getDecoratedVehicle();
          }
        else
          {
            if(innerVehicle instanceof AbstractVehicleOption)
              {
                ((AbstractVehicleOption)innerVehicle).removeDecorator(innerVehicle, decoratorName);
              }
          }

        //der äußerste Dekorierer muß entfernt werden
        if(vehicle.getClass().getSimpleName().equals(decoratorName))
          {
            return ((AbstractVehicleOption)vehicle).getDecoratedVehicle();
          }

        return vehicle;
      }
}
