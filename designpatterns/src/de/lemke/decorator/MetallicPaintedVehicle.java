package de.lemke.decorator;

import de.lemke.vehicle.*;

public class MetallicPaintedVehicle extends AbstractVehicleOption
{
    // Konstruktor
    public MetallicPaintedVehicle(Vehicle vehicle)
      {
        super(vehicle);
      }

    // Metalliclack kostet Aufpreis
    @Override
    public int getPrice()
      {
        return this.decoratedVehicle.getPrice() + 700;
      }

    @Override
    public int getDailyRate()
      {
        return this.decoratedVehicle.getDailyRate() + 4;    // Metalliclackierung kostet 4,- € mehr pro Tag
      }

    // hier folgen die optionsspezifischen Methoden ...
}
