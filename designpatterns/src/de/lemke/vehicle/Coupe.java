package de.lemke.vehicle;

import de.lemke.engine.*;

public class Coupe extends AbstractCar
{
  //Konstruktor
  public Coupe(Engine engine)
    {
      super(engine);
    }

  //Konstruktor
  public Coupe(Engine engine, Vehicle.Colour colour)
    {
      super(engine,colour);
    }

  @Override
  public int getPrice()
    {
      return 7000;
    }

  @Override
  public int getDailyRate()
    {
      return 59;
    }
}
