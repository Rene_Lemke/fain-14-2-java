package de.lemke.vehicle;

import de.lemke.engine.*;

public class BoxVan extends AbstractVan
{
  //Konstruktor
  public BoxVan(Engine engine)
    {
      super(engine);
    }

  //Konstruktor
  public BoxVan(Engine engine, Vehicle.Colour colour)
    {
      super(engine,colour);
    }

  @Override
  public int getPrice()
    {
      return 10000;
    }

  @Override
  public int getDailyRate()
    {
      return 89;
    }
}
