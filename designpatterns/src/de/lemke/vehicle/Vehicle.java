package de.lemke.vehicle;

import de.lemke.engine.*;

public interface Vehicle extends Cloneable
{
  //verschachtelte Klasse, nested class
  //Farben dann später verwendbar als z.B. Vehicle.Colour.RED
  public enum Colour
    { UNPAINTED, BLUE, BLACK, GREEN, RED, SILVER, WHITE, YELLOW };

  public Engine getEngine();                           //Engine wird hier injiziert
  public Vehicle.Colour getColour();
  public void paint(Vehicle.Colour colour);

  //die Methode clone des Interface Cloneable (damit darf für alle Vehicle die Methode clone aufgerufen werden)
  public Object clone();

  //Methode zum Üben des Bridge-Patterns
  public void move(int km);

  //Methode(n) für das Dekorierer-Pattern
  public int getPrice();

  public int getDailyRate();     // Tagessatz für den Verleih

  //Methoden für das Fassade-Pattern
  public void cleanInterior();
  public void cleanExterior();
  public void polishWindows();
  public void testDrive();
}
