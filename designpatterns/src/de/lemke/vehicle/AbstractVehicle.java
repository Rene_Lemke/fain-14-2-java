package de.lemke.vehicle;

import de.lemke.engine.*;
import de.lemke.component.*;

public abstract class AbstractVehicle implements Vehicle
{
  //gemeinsamen Eigenschaften der Fahrzeuge
  private Engine engine;
  private Vehicle.Colour colour;
  private Window window;

  protected int mileage;

  //Konstruktor
  public AbstractVehicle(Engine engine)
    {
      this(engine, Vehicle.Colour.UNPAINTED);     // klassenintern wird der andere Konstruktor aufgerufen
      this.mileage = 0;
    }

  public AbstractVehicle(Engine engine, Vehicle.Colour colour)
    {
      this.engine = engine;
      this.colour = colour;
      this.mileage = 0;
    }

  // ermittelt den Basistagessatz für die Verleihsituation (Decorator-Pattern)
  @Override
  public int getDailyRate()
    {
      return 0;
    }

  @Override
  public Engine getEngine()
    {
      return this.engine;
    }

  @Override
  public Vehicle.Colour getColour()
    {
      return this.colour;
    }

  @Override
  public void paint(Vehicle.Colour colour)
    {
      this.colour = colour;
    }

  public void addWindow(Window window)
    {
      this.window = window;
    }

  @Override
  public String toString()
    {
      StringBuilder sb = new StringBuilder();

      sb.append(String.format("%s (%s, %s), Kaufpreis: %d, Mietpreis/Tag: %d", this.getClass().getSimpleName(), this.engine, this.colour, this.getPrice(), this.getDailyRate()));

      if(this.window != null)
        {
          sb.append(String.format(" %s", this.window));
        }
      return sb.toString();
    }

  @Override
  public Object clone()
   {
     Object obj = null;

     try
      {
        //die Methode clone() der BasisKlasse wird aufgerufen
        obj = super.clone();
      }
      catch(CloneNotSupportedException e)
       {
         System.err.println(e);
       }

      return obj;
    }

   @Override
   public void move(int km)
     {
       if(km > 0)
         {
          this.mileage += km;
          System.out.println("neuer Kilometerstand: " + this.mileage);
         }
     }

   @Override
   public void cleanInterior()
     {
       System.out.println("Reinige den Innenraum ...");
     }

   @Override
   public void cleanExterior()
     {
       System.out.println("Reinige das Fahrzeug von außen");
     }

   @Override
   public void testDrive()
     {
       //wir fahren 2 km
       this.move(2);

       System.out.println("Testfahrt ausgeführt");
     }

    @Override
    public void polishWindows()
     {
       System.out.println("Sidolin streifenfrei...");
     }
}
