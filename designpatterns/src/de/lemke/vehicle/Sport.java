package de.lemke.vehicle;

import de.lemke.engine.*;

public class Sport extends AbstractCar
{
  //Konstruktor
  public Sport(Engine engine)
    {
      super(engine);
    }

  //Konstruktor
  public Sport(Engine engine, Vehicle.Colour colour)
    {
      super(engine,colour);
    }

  @Override
  public int getPrice()
    {
      return 110000;
    }

  @Override
  public int getDailyRate()
    {
      return 79;
    }
}
