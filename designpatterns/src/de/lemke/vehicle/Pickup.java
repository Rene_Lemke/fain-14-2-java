package de.lemke.vehicle;

import de.lemke.engine.*;

public class Pickup extends AbstractVan
{
  //Konstruktor
  public Pickup(Engine engine)
    {
      super(engine);
    }

  //Konstruktor
  public Pickup(Engine engine, Vehicle.Colour colour)
    {
      super(engine,colour);
    }

  @Override
  public int getPrice()
    {
      return 9000;
    }

  @Override
  public int getDailyRate()
    {
      return 79;
    }
}
