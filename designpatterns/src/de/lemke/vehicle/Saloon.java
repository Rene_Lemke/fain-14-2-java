package de.lemke.vehicle;

import de.lemke.engine.*;

public class Saloon extends AbstractCar
{
  //Konstruktor
  public Saloon(Engine engine)
    {
      super(engine);
    }

  //Konstruktor
  public Saloon(Engine engine, Vehicle.Colour colour)
    {
      super(engine,colour);
    }

  @Override
  public int getPrice()
    {
      return 6000;
    }

  @Override
  public int getDailyRate()
    {
      return 49;
    }
}
