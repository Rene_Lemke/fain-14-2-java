package de.lemke.iterator;

import java.util.Iterator;

public class PrintBrochure
{
    //der Method wird ein Iterator übergeben
    public static void printIterator(Iterator itr)
    {
        System.out.println("====== Auflistung ======");

        //solange noch ein nächstes Element in der Liste vorhanden ist...
        while(itr.hasNext())
        {
            //...gebe das nächste Element wieder
            System.out.println(itr.next());
        }
    }
}
