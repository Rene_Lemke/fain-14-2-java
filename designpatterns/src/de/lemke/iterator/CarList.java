package de.lemke.iterator;

import de.lemke.engine.*;
import de.lemke.vehicle.*;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class CarList implements Iterable<Vehicle>
{
  //die Autos werden intern durch eine List<E> repräsentiert
  private List<Vehicle> cars;

  public CarList()
  {
      cars = new ArrayList<>();

      //ein paar Autos hinzufügen
      cars.add(new Saloon(new StandardEngine(1300)));
      cars.add(new Coupe(new TurboEngine(2300)));
      cars.add(new Sport(new TurboEngine(3000)));
      cars.add(new Saloon(new StandardEngine(1100)));
  }

  //damit die Liste verwendet werden kann
  public List<Vehicle> getList()
   {
      return cars;
   }

  @Override
  public Iterator<Vehicle> iterator()
    {
        //die List<E> implementiert bereits das Iterator-Interface
        //und bietet eine Methode an, um den Interator zu erzeugen
        return cars.listIterator();
    }
}
