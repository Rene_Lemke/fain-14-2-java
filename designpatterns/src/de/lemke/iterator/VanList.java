package de.lemke.iterator;

import de.lemke.vehicle.*;
import de.lemke.engine.*;

import java.util.Arrays;
import java.util.Iterator;


public class VanList implements Iterable<Vehicle>
{
    //Vans werden intern in einem Array erfasst
    private Vehicle[] vans = new Vehicle[3];

    public VanList()
    {
        vans[0] = new Pickup(new StandardEngine(3000));
        vans[1] = new BoxVan(new StandardEngine(2800));
        vans[2] = new Pickup(new TurboEngine(4000));
    }

    //damit jemand an die Van-Liste gelangt
    public Vehicle[] getList()
    {
        return vans;
    }

    @Override
    public Iterator<Vehicle> iterator()
     {
        //das Array selbst wäre auch iterierbar
        return Arrays.asList(vans).listIterator();
     }
}
