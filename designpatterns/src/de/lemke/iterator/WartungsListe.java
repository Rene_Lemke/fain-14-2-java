package de.lemke.iterator;

import de.lemke.vehicle.Vehicle;
import java.util.Iterator;
import java.util.LinkedList;

public class WartungsListe implements Iterable<Vehicle>
{
    private LinkedList<Vehicle> vqueue;

    public WartungsListe()
     {
      vqueue = new LinkedList<>();
     }

    public void register(Vehicle v)
    {
        vqueue.add(v);
    }

    public Vehicle executeMaintenance()
    {
        return vqueue.pollFirst();
    }

    @Override
    public Iterator<Vehicle> iterator()
    {
        return vqueue.listIterator();
    }
}
