package de.lemke.bridge;

import de.lemke.vehicle.*;

public abstract class AbstractGameControls
{
  private Vehicle vehicle;

  //über den Konstruktor wird das zu steuernde Fahrzeug injiziert
  public AbstractGameControls(Vehicle v)
    {
      this.vehicle = v;
    }

  // der Spielzug
  public void turn()
    {
      this.vehicle.move(1);
    }
}
