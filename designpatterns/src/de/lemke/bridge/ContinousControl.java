package de.lemke.bridge;

import de.lemke.vehicle.*;

public class ContinousControl extends AbstractGameControls
{
  // über den Konstruktor wird das zu steuernde Fahrzeug injiziert
  public ContinousControl(Vehicle v)
    {
      super(v);
    }

  // der Spielzug
  public void specialTurn(int count)
    {
      System.out.println("continous control");

      while(count > 0)
        {
          this.turn();
          count -= 1;
        }
    }
}
