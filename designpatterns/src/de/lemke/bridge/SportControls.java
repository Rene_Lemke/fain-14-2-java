package de.lemke.bridge;

import de.lemke.engine.*;

public class SportControls extends AbstractDriverControls
{
  //Konstruktor
  public SportControls(Engine engine)
    {
      super(engine);
    }

  // ein neues Feature
  public void accelerateHard()
    {
      // das neue Interface basiert auf den Methoden der AbstractDriverControls
      // Hinweis: ein direkter Aufruf der Methoden der Engine-Hierarchie wäre nicht im Sinne
      // des Musters.
      this.accelerate();
      this.accelerate();
    }
}
