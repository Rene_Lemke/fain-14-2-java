package de.lemke.bridge;

import de.lemke.vehicle.*;

public class TurnBasedControl extends AbstractGameControls
{
    //über den Konstruktor wird das zu steuernde Fahrzeug injiziert
    public TurnBasedControl(Vehicle v)
      {
        super(v);
      }

   @Override
   public void turn()
     {
       System.out.println("turnbased control");
       super.turn();
     }
}
