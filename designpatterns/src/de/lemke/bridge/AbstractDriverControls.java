package de.lemke.bridge;

import de.lemke.engine.*;

public abstract class AbstractDriverControls
{
  //Objekte dieser Klasse steuern Motoren
  private Engine engine;

  //Konstruktor
  public AbstractDriverControls(Engine engine)
    {
      //der zu steuernde Motor wird injiziert
      this.engine = engine;
    }

  // die Methoden der neuen Abstraktion laut Bridge-Pattern
  // rufen dann die Original-Methoden der Engine-Hierarchie auf

  public void ignitionOn()
   {
     this.engine.start();
   }

  public void ignitionOff()
   {
     this.engine.stop();
   }

  public void accelerate()
   {
     this.engine.increasePower();
   }

  public void brake()
   {
     this.engine.decreasePower();
   }
}
