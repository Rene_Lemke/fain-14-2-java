package de.lemke.bridge;

import de.lemke.engine.*;

public class DisabledControls extends AbstractDriverControls
{
  //Konstruktor
  public DisabledControls(Engine engine)
    {
      super(engine);
    }

  //ein neues Features

  public void voiceActivation()
    {
      System.out.println("voice activation");
      this.ignitionOn();
    }

  public void voiceDeactivation()
    {
      System.out.println("voice deactivation");
      this.ignitionOff();
    }
}
