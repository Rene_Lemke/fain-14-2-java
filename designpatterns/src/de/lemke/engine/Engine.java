package de.lemke.engine;

import de.lemke.flyweight.*;

public interface Engine
{
  //liefert den Hubraum
  public int getSize();

  //liefert true, falls es sich um einen Turbomotor handelt
  public boolean isTurbo();


  // die vier Methoden dienen der Veranschaulichung des Bridge-Patterns
  // der Aufruf der Methoden ändert den Zustand eines Motors

  public void start();
  public void stop();
  public void increasePower();
  public void decreasePower();

  //Methoden für die Fassade
  public void checkOil();
  public void testRun();
  public void getDocuments();
  public void cleaning();

  //Methode, die bei dem Flyweight-Pattern eingeführt wurde
  public void diagnose(DiagnosticTool tool);
}
