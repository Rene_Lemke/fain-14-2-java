package de.lemke.engine;

public class ElectricEngine extends AbstractEngine
{
    //Konstruktor
    public ElectricEngine()
      {
        //kein Hubraum, keine Turboaufladung
        super(0,false);
      }

    public void start()
      {
        //nichts zu tun: ein E-Motor kann nicht angelassen werden
        System.out.println(".......");
      }

    public void stop()
      {
        //nichts zu tun: ein E-Motor kann nicht abgeschaltet werden
        System.out.println(".......");
      }

    public void increasePower()
      {
        this.currentPower += 1;
        System.out.println("E-Power increased to " + this.currentPower);
      }

    public void decreasePower()
      {
        this.currentPower -= 1;
        System.out.println("E-Power decreased to " + this.currentPower);
      }
}
