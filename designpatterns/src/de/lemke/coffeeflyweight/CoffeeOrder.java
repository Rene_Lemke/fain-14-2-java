package de.lemke.coffeeflyweight;

public interface CoffeeOrder
{
  public void serveCoffee(CoffeeOrderContext context);
}
