package de.lemke;

import de.lemke.engine.*;
import de.lemke.vehicle.*;

//Erzeugungsmuster
import de.lemke.singleton.*;
import de.lemke.factory.*;
import de.lemke.builder.*;
import de.lemke.abstractfactory.*;
import de.lemke.prototype.*;

//Strukturmuster
import de.lemke.adapter.*;
import de.lemke.composite.*;
import de.lemke.bridge.*;
import de.lemke.decorator.*;
import de.lemke.fassade.*;
import de.lemke.flyweight.*;
import de.lemke.proxy.*;

//Verhaltensmuster
import de.lemke.observer.*;
import de.lemke.iterator.*;
import de.lemke.command.*;
import de.lemke.state.*;

import java.util.*;

public class MainApp {
    public static void main(String[] args) {
        System.out.println(new Coupe(new TurboEngine(2200), Vehicle.Colour.RED));

        System.out.println(new Pickup(new StandardEngine(3000)));

        //diese Methode ist die einzige Möglichkeit, an die Singleton-Instanz zu gelangen
        SerialNumberGenerator sng = SerialNumberGenerator.getInstance();          // der new SerialNumberGenerator kann nicht mehr verwendet werden

        //Stresstest
        SerialNumberGenerator sngNotACopy = SerialNumberGenerator.getInstance();

        System.out.format("Seriennummer: %04d %n", sng.getNextSerial());
        System.out.format("Seriennummer: %04d %n", sngNotACopy.getNextSerial());
        System.out.format("Seriennummer: %04d %n", sng.getNextSerial());
        System.out.format("Seriennummer: %04d %n", sngNotACopy.getNextSerial());
        System.out.format("Seriennummer: %04d %n", sng.getNextSerial());
        System.out.format("Seriennummer: %04d %n", sngNotACopy.getNextSerial());

        System.out.format("sng: %d, sngNotACopy: %d %n", sng.hashCode(), sngNotACopy.hashCode());

        //hier der Test des EnumSingletons
        System.out.format("-------%nV-Seriennummer: %04d %n", EnumSerialNumberGenerator.VEHICLE.getNextSerial());
        System.out.format("E-Seriennummer: %04d %n", EnumSerialNumberGenerator.ENGINE.getNextSerial());
        System.out.format("V-Seriennummer: %04d %n", EnumSerialNumberGenerator.VEHICLE.getNextSerial());
        System.out.format("E-Seriennummer: %04d %n", EnumSerialNumberGenerator.ENGINE.getNextSerial());
        System.out.format("V-Seriennummer: %04d %n", EnumSerialNumberGenerator.VEHICLE.getNextSerial());
        System.out.format("E-Seriennummer: %04d %n", EnumSerialNumberGenerator.ENGINE.getNextSerial());
        System.out.format("V-Seriennummer: %04d %n", EnumSerialNumberGenerator.VEHICLE.getNextSerial());
        System.out.format("E-Seriennummer: %04d %n", EnumSerialNumberGenerator.ENGINE.getNextSerial());

      /*
        Fabrik/Fabrikmethode
      */

        AbstractVehicleFactory fab = new CarFactory();

        System.out.format("%s %n", fab.build(AbstractVehicleFactory.DrivingStyle.ECO, Vehicle.Colour.BLUE));
        System.out.format("%s %n", fab.build(AbstractVehicleFactory.DrivingStyle.MID, Vehicle.Colour.RED));
        System.out.format("%s %n", fab.build(AbstractVehicleFactory.DrivingStyle.POWER, Vehicle.Colour.SILVER));

        //und ein VanFactoryfab = new VanFactory();
        fab = new VanFactory();

        System.out.format("%s %n", fab.build(AbstractVehicleFactory.DrivingStyle.POWER, Vehicle.Colour.WHITE));
        System.out.format("%s %n", fab.build(AbstractVehicleFactory.DrivingStyle.ECO, Vehicle.Colour.BLUE));

        //die Variante:
        System.out.format("%s %n", VehicleFactoryVariante.make(VehicleFactoryVariante.Category.CAR, AbstractVehicleFactory.DrivingStyle.POWER, Vehicle.Colour.YELLOW));

        /**
         *    Erbauer / builder
         **/
        //ein Auto im halbfertigen Zustand (inkl. Motor)
        AbstractCar car = new Saloon(new StandardEngine(1300));

        //ein Builderobjekt, daß an das halbfertige Auto Komponenten dranschraubt
        VehicleBuilder builder = new CarBuilder(car);

        //der Direktor steuert den Produktionsprozess
        VehicleDirector director = new CarDirector();

        //das fertige Fahrzeug
        Vehicle v = director.build(builder);

        System.out.println(v);

        //ein Builderobjekt, daß an den halbfertigen Van Komponenten ranschraubt
        VehicleBuilder vanbuilder = new VanBuilder(new Pickup(new StandardEngine(3000)));

        //der Director steuert den Produktionsprozess
        VehicleDirector vandirector = new VanDirector();

        //der fertige Van
        Vehicle van = vandirector.build(vanbuilder);

        System.out.println(van);

      /*
      *  Abstrakte Fabrik
      */
        //wir erzeugen uns eine Instanz der Car-AbstrakteFabrik
        AbstractFactory abstractFab = new CarAbstractFactory();

        //wir lassen die Produkte erzeugen
        System.out.println(abstractFab.createBody().getBodyParts());              //fluent interface
        System.out.println(abstractFab.createChassis().getChassisParts());
        System.out.println(abstractFab.createWindows().getWindowsParts());

        //und nun der Trick
        abstractFab = new VanAbstractFactory();                                   //andere Fabrik -> andere Produkte

        //wir lassen die Produkte erzeugen
        System.out.println(abstractFab.createBody().getBodyParts());              //fluent interface
        System.out.println(abstractFab.createChassis().getChassisParts());
        System.out.println(abstractFab.createWindows().getWindowsParts());

      /*
      *  Prototype
      */
        Vehicle vorlage = new Saloon(new StandardEngine(1100));

        Vehicle kopie = (Vehicle) vorlage.clone();     // wir klonen die Vorlage und casten auf Vehicle

        System.out.println("Original: " + vorlage);
        System.out.println("Kopie: " + kopie);

        kopie.paint(Vehicle.Colour.RED);
        System.out.println("Kopie:    " + kopie);

        //wir lassen den VehicleManager Fahrzeuge erzeugen durch klonen
        VehicleManager vhm = new VehicleManager();

        System.out.println(vhm.createSport());
        System.out.println(vhm.createPickup());

        //wir lassen den VehicleManagerLazy Fahrzeuge erzeugen durch klonen
        VehicleManagerLazy vhml = new VehicleManagerLazy();

        System.out.println(vhml.createCoupe());
        System.out.println(vhml.createBoxVan());

        //Laufzeitverhalten new vs. Prototype klonen

        long start;
        long ende;
        long diffA, diffB;

        int max = 10000;

        start = System.nanoTime();
        //Beginn der Zeitmessung
        for (int i = 0; i < max; i++) {
            new Saloon(new StandardEngine(1300));
        }
        //Ende der Zeitmessung
        ende = System.nanoTime();

        diffA = ende - start;

        start = System.nanoTime();
        //Beginn der Zeitmessung
        for (int i = 0; i < max; i++) {
            vhm.createSaloon();
        }
        //Ende der Zeitmessung
        ende = System.nanoTime();

        diffB = ende - start;

        System.out.format("Messung 1: %d ns, Messung 2: %s ns, ratio: %3.2f%n", diffA, diffB, (double) diffB / (double) diffA);


        /*******************************************************************************************
         *                     Strukturmuster
         *
         ********************************************************************************************/

      /*
         Adapter
     */

        List<Engine> engines = new ArrayList<Engine>();

        engines.add(new StandardEngine(1300));
        engines.add(new StandardEngine(1600));
        engines.add(new StandardEngine(1800));
        engines.add(new StandardEngine(2000));

        engines.add(new TurboEngine(2000));
        engines.add(new TurboEngine(2200));
        engines.add(new TurboEngine(2500));
        engines.add(new TurboEngine(3000));

        //Aufnehmen einer adaptierten GreenEngine in die Liste
        engines.add(new AdapterEngine(new GreenEngine(1100)));

        //und noch eine
        engines.add(new AdapterEngineAlternative(new GreenEngine(900)));

        //da die Standard- und Turbomotoren in der gleichen Hierarchie unterhalb von Engine liegen,
        //können wir die Motoren auf die gleiche Art und Weise verwenden.

        for (Engine e : engines) {
            System.out.println(e);
        }

        //Übung zu den Adaptern Truck -> Vehicle
        List<Vehicle> vehicles = new ArrayList<Vehicle>();

        vehicles.add(new Sport(new TurboEngine(3000), Vehicle.Colour.SILVER));
        vehicles.add(new Coupe(new StandardEngine(2000), Vehicle.Colour.WHITE));
        vehicles.add(new Saloon(new StandardEngine(1100), Vehicle.Colour.BLACK));

        vehicles.add(new Pickup(new TurboEngine(3000)));

        vehicles.add(new AdapterVehicle(new DumpTruck(40, new TurboEngine(3300))));
        vehicles.add(new AdapterVehicleAlternative(new SemitrailerTruck(40, new TurboEngine(3000))));

        for (Vehicle vehicle : vehicles) {
            System.out.println(vehicle);
        }

        /**
         *    Composite-Pattern
         */

        Item nut = new Part("Nut", 5);
        Item bolt = new Part("Bolt", 9);
        Item panel = new Part("Panel", 35);

        Item compA = new Component("Komponente A");
        compA.addItem(nut);
        compA.addItem(bolt);
        compA.addItem(panel);

        Item compB = new Component("Komponente B");
        compB.addItem(compA);
        compB.addItem(nut);

        Item compC = new Component("Komponente C");
        compC.addItem(compB);

        System.out.println(nut);
        System.out.println(bolt);
        System.out.println(panel);

        System.out.println(compA);
        System.out.println(compB);

        compC.ausgabe();


     /*
     *   Bridge-Pattern
     */

        Engine bridgeEngine = new StandardEngine(1800);

        //wir setzen die "normale" Steuerung ein
        StandardControls stdCtl = new StandardControls(bridgeEngine);

        stdCtl.ignitionOn();        // Motor anlassen
        stdCtl.accelerate();      // beschleunigen
        stdCtl.brake();           // verzögern bzw. bremsen
        stdCtl.ignitionOff();     // Motor ausschalten

        //wir setzen die "sportive" Steuerung ein
        SportControls sptCtl = new SportControls(bridgeEngine);

        sptCtl.ignitionOn();
        sptCtl.accelerate();
        sptCtl.accelerateHard();  // ordentlich Gas geben
        sptCtl.brake();
        sptCtl.ignitionOff();

        //wir setzen die Steuerung für behinderte Menschen ein
        DisabledControls dsbCtl = new DisabledControls(bridgeEngine);

        dsbCtl.voiceActivation();
        dsbCtl.accelerate();
        dsbCtl.brake();
        dsbCtl.voiceDeactivation();

        //die Spielsteuerung testeb
        Vehicle gameCar = new Saloon(new StandardEngine(1600));

        TurnBasedControl tbCtl = new TurnBasedControl(gameCar);
        tbCtl.turn();

        ContinousControl cnCtl = new ContinousControl(gameCar);
        cnCtl.specialTurn(10);

     /*
     *   Dekorierer-Pattern
     */

        Vehicle decoCar = new Saloon(new StandardEngine(1600));
        System.out.println(decoCar);

        //wir dekorieren das Fahrzeug mit einer Klimaanlage
        decoCar = new AirConditionedVehicle(decoCar);
        System.out.println(decoCar);

        //wir dekorieren das Fahrzeug mit Ledersitzen
        decoCar = new LeatherSeatedVehicle(decoCar);
        System.out.println(decoCar);

        // den äußeren Dekodierer entfernen
        System.out.format("Dekorierer [%s] wird entfernt.%n%n", AirConditionedVehicle.class.getSimpleName());
        decoCar = ((AbstractVehicleOption) decoCar).removeDecorator(decoCar, AirConditionedVehicle.class.getSimpleName());

        System.out.println(decoCar);

     /*
     *   Fassade-Pattern
     */
        System.out.println("---- Fassade ----");

        Engine eng = new TurboEngine(3000);

        //den Motor für den Einbau vorbereiten
        EngineFassade.prepareForAssembly(eng);

        //den vorbereiteten Motor einbauen
        Vehicle fassadeVehicle = new Saloon(eng);

        //das Fahrzeug für den Verkauf vorbereiten
        VehicleFassade vf = new VehicleFassade();
        vf.prepareForSale(fassadeVehicle);

     /*
     *    Flyweight-Pattern
     */

        //wir erzeugen eine Instanz der Fliegengewichtfabrik
        EngineFlyweightFactory eff = new EngineFlyweightFactory();

        //wir erzeugen ein Fahrzeug unter Einsatz der EFF
        Vehicle effVehicle01 = new Saloon(eff.getStandardEngine(1200));

        //wir erzeugen ein Fahrzeug unter Einsatz der EFF
        Vehicle effVehicle02 = new Pickup(eff.getTurboEngine(3000));

        //wir erzeugen ein Fahrzeug unter Einsatz der EFF, diesmal mit einer Engine, die bereits erzeugt worden ist
        //Vehicle 03 und Vehicle01 teilen sich ein Motorobjekt
        Vehicle effVehicle03 = new Coupe(eff.getStandardEngine(1200));

        //Kontrollausgabe
        System.out.format("Motor von V01: %d, Motor von V03 %d%n%n", effVehicle01.getEngine().hashCode(), effVehicle03.getEngine().hashCode());

        //wir testen unsere Engines mit dem DiagnoseTool

        Engine effEng01 = eff.getStandardEngine(1200);
        Engine effEng02 = eff.getTurboEngine(3000);
        Engine effEng03 = eff.getTurboEngine(2000);
        Engine effEng04 = eff.getTurboEngine(3000);
        Engine effEng05 = eff.getStandardEngine(1200);

        DiagnosticTool tool = new EngineDiagnosticTool();

        effEng01.diagnose(tool);
        effEng02.diagnose(tool);
        effEng03.diagnose(tool);
        effEng04.diagnose(tool);
        effEng05.diagnose(tool);

        System.out.format("%d%n", effEng01.hashCode());
        System.out.format("%d%n", effEng02.hashCode());
        System.out.format("%d%n", effEng03.hashCode());
        System.out.format("%d%n", effEng04.hashCode());
        System.out.format("%d%n", effEng05.hashCode());


    /*
    *   Proxy-Pattern
    */
        //wir erzeugen ein Stellvertreterobjekt mit 1600 ccm Hubraum und einer StandardEngine
        ProxyVehicle vp01 = new ProxyVehicle(1600, false);

        //wir fahren ohne Guthaben los
        vp01.move(10);

        //wir kaufen Guthaben
        vp01.guthabenAufladen(100);

        vp01.move(90);
        vp01.move(20);

     /*
     *   Beobachter-Muster
     */
        // 2 Rollen: 1x Beobachter, 1x Beobachteter

        //wir erzeugen uns einen Monitor (Beobachter)
        SpeedMonitor monitor = new SpeedMonitor();

        //wir erzeugen uns eine GearBox (Beobachter)
        GearBox gearbox = new GearBox();

        //wir erzeugen uns ein Speedometer-Objekt (Beobachteter)
        Speedometer speedo = new Speedometer();

        //der Monitor wird beim Speedometer registriert
        speedo.addObserver(monitor);

        //die Gearbox wird beim Speedometer registriert
        speedo.addObserver(gearbox);

        //wir setzen verschiedene Geschwindigkeiten
        for (int velo = 10; velo < 100; velo += 10) {
            speedo.setCurrentSpeed(velo);
        }

        //unser eigenes Beobachtermuster außerhalb der Standard-Javaklassen
        ObservedVehicle ov = new ObservedVehicle(new StandardEngine(1300));

        //die Inspektoren erzeugen
        ErstInspektor ei = new ErstInspektor();
        IntervallInspektor ii = new IntervallInspektor();

        //Beobachter hinzufügen
        ov.addObserver(ei);
        ov.addObserver(ii);

        for(int count=0; count < 20; count++)
        {
            //das Fahrzeug 1000 km fahren lassen
            ov.move(1000);
        }

        /**
         *   Iterator-Pattern
         */
        System.out.format("\n");
        CarList carList = new CarList();
        VanList vanList = new VanList();

        PrintBrochure.printIterator(carList.iterator());
        PrintBrochure.printIterator(vanList.iterator());

        WartungsListe wartungsListe = new WartungsListe();

        wartungsListe.register(new Coupe(new StandardEngine(1800)));
        wartungsListe.register(new Pickup(new StandardEngine(3000)));
        wartungsListe.register(new Sport(new StandardEngine(2200)));
        wartungsListe.register(new Saloon(new StandardEngine(1100)));
        wartungsListe.register(new Sport(new TurboEngine(2200)));
        wartungsListe.register(new BoxVan(new StandardEngine(1100)));

        PrintBrochure.printIterator(wartungsListe.iterator());

        //weil das Iterable-Interface implementiert wird, geht auch for-each
        for(Vehicle mv: wartungsListe)
         {
           System.out.format("(for-each): %s%n", mv);
         }

        Iterator<Vehicle> itr = wartungsListe.iterator();
        while(itr.hasNext())
         {
           System.out.format("(while): %s%n", itr.next());

           //ACHTUNG!
           //Es wird eine Wartung durchgeführt.
           // -->das Fahrzeug wird aus der Wartungsliste entfernt
           // -->die ursprüngliche Liste war aber die Basis für den Iterator
           // wartungsListe.executeMaintenance();
         }

        /*
		 *			Command-Pattern
		 */

        System.out.format("\n");

        //wir erzeugen uns ein Objekt für die Sprachsteuerung
        SpeechControl speechCtl = new SpeechControl();

        Radio radio = new Radio();

        //Command-Objekte für das Radio
        VolumeUp radioCtlUp = new VolumeUp(radio);
        VolumeDown radioCtlDown = new VolumeDown(radio);
        RadioOn radioCtlOn = new RadioOn(radio);
        RadioOff radioCtlOff = new RadioOff(radio);

        //wir injizieren VolumeUp und VolumeDown in die Sprachsteuerung
        speechCtl.setCommands(radioCtlUp, radioCtlDown, radioCtlOn, radioCtlOff);

        System.out.println("\n----Testen der Lautstärke-----");

        //...und steuern das Radio mittels Sprache
        speechCtl.hearOn();   //Radio anstellen
        speechCtl.hearUp();   //lauter stellen
        speechCtl.hearUp();   //lauter stellen
        speechCtl.hearUp();   //lauter stellen

        speechCtl.hearUndo();  //das Letzte Kommando rückgängig machen

        speechCtl.hearDown();   //leiser stellen

        speechCtl.hearOff();    //Radio ausstellen

        //Testen der automatischen Fensterheber
        System.out.println("\n----Testen der Fensterheber-----");
        Window window = new Window();

        WindowUp windowCtlUp = new WindowUp(window);
        WindowDown windowCtlDown = new WindowDown(window);

        //wir schalten die Sprachsteuerung auf die Fenster um
        speechCtl.setCommands(windowCtlUp, windowCtlDown);

        speechCtl.hearDown();  //Fenster wird geöffnet
        speechCtl.hearUp();    //Fenster wird geschlossen

        speechCtl.hearUndo();  //das Letzte Kommando rückgängig machen

        /**
         *      State-Pattern
        **/

        System.out.format("\n");

        //die Context-Klasse instanziieren
        ClockSetup clockSetup = new ClockSetup();

        //das Jahr einstellen
        clockSetup.rotateButtonRight();               //Jahr + 1
        clockSetup.pushButton();                      //Zustandswechsel auslösen

        //den Monat einstellen
        clockSetup.rotateButtonLeft();                //Monat - 1
        clockSetup.pushButton();                      //Zustandswechsel auslösen

        //den Tag einstellen
        clockSetup.rotateButtonRight();               //Tag - 1
        clockSetup.rotateButtonRight();               //Tag - 1
        clockSetup.rotateButtonRight();               //Tag - 1
        clockSetup.pushButton();                      //Zustandswechsel auslösen

        //die Stunde einstellen
        clockSetup.rotateButtonLeft();                //Stunde - 1
        clockSetup.pushButton();                      //Zustandswechsel auslösen

        //die Minuten einstellen
        clockSetup.rotateButtonRight();               //Minute + 1
        clockSetup.pushButton();                      //Zustandswechsel auslösen

        //der finale Zustand liefert das komplette eingestellte Datum
        clockSetup.pushButton();
    }
}
