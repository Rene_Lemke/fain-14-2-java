package de.lemke;

/**
 *   Die Klasse Cell repräsentiert den kleinsten Bestandteil des Organismus.
 *   Eine Zelle kann lebendig oder tot sein.
 *   Jede Zelle hat einen aktuellen Zustand für die aktuelle Generation und
 *   einen zukünftigen Zustand für die nächste Generation.
 */
public class Cell
{
    //aktueller Zustand der Zelle
    private boolean current;

    //Zustand der Zelle in der nächsten Generation
    private boolean future;

    /**
     * Konstruktor, der den Schwellenwert bestimmt, mit welcher Wahrscheinlichkeit die neue Zelle
     * lebendig oder tot ist.
     */
    public Cell(float threshold)
    {
        //eine lebende Zelle entsteht, wenn der Zufallswert größer ist als der Schwellwert
        current = Math.random() > threshold;
        //für die zukünftige Generation wird erstmal angenommen, daß der aktuelle Zustand auch der zukünftige ist
        future = current;
    }

    //liefert den aktuellen Zustand
    public boolean isAlive()
    {
        return current;
    }

    //Zelle stirbt in der nächsten Generation
    public void isDying()
    {
        future = false;
    }

    //Zelle wird in der nächsten Generation wiedergeboren
    public void isLiving()
    {
        future = true;
    }

    //die neue Generation wird zur aktuellen Generation
    public void propagate()
    {
        current = future;
    }
}
