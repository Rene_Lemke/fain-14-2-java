package de.lemke;

import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class Organism
{
    //der grafische Kontext wird in das Organ injiziert
    private GraphicContext gc;

    //Menge an Zellen als zweidimensionales Array
    private Cell[][] cells;

    //die Abmaße einer quadratischen Zelle auf dem Bildschirm
    private int dim;
    //der Abstand zwischen den Zellen
    private int step;

    //der Standardschwellenwert für eine neue Zelle
    private final float DEFAULT = 0.5f;
    //der Schwellwert für eine tote Zelle (Random > 1.0 geht nicht)
    private final float DEAD = 1.0f;
    //der Schwellwert für eine lebendige Zelle (Random < 0 geht nicht)
    private final float ALIVE = -1.0f;


    //Konstruktor
    public Organism(GraphicContext context, int d, int s)
    {
        //Zellgröße
        dim = d;
        //Zellabstand
        step = s;
        //grafischer Kontext
        gc = context;

        //wie groß muß der Organismus sein - Bildschirmbreite / (Größe der Zelle + Abstand der Zellen)
        int reihen = gc.getCurrentHeight() / (dim + step);
        // Bildschirmhöhe / (Größe der Zelle + Abstand der Zellen)
        int spalten = gc.getCurrentWidth() / ( dim + step );

        //erzeugen des Organismus
        cells = new Cell[reihen][spalten];

        //erzeuge eine Zufallsbelegung für den Organismus
        createGeneration(DEFAULT);

        //liest eine Zellbelegung aus einer Datei
        //loadFromFile("eingabe.txt");
    }

    /**
     * Erzeuge eine Zufallsbelegung für den Organismus
     */
    private void createGeneration(float threshold)
    {
        int zellReihen = cells.length;
        int zellSpalten = cells[0].length;

        for(int reihe = 0; reihe < zellReihen; reihe++ )
        {
            for(int spalte = 0; spalte < zellSpalten; spalte++ )
            {
                cells[reihe][spalte] = new Cell(threshold);
            }
        }
    }

    /**
    *  Berechnen der neuen Generation (jede Zelle speichert den neuen Zustand in der future-Eigenschaft
    */
    public void nextGeneration()
    {
        int count = 0;
        int ngReihen = cells.length;
        int ngSpalten = cells[0].length;

        for(int reihe = 0; reihe < ngReihen; reihe++)
        {
            for(int spalte = 0; spalte < ngSpalten; spalte++)
            {
                count = countNeighbours(reihe,spalte);

                //falls die Zelle lebt
                if(cells[reihe][spalte].isAlive())
                {
                    //stirbt sie an Vereinsamung oder Überbevölkerung
                    if(count < 2 || count > 3)
                        cells[reihe][spalte].isDying();
                }
                else
                {
                    if(count == 3)
                        cells[reihe][spalte].isLiving();
                }
            }
        }

        //neuen Zustand setzen (die current-Eigenschaft der Zelle erhält den Wert der future-Eigenschaft)
        for(int reihe = 0; reihe < cells.length; reihe++)
        {
            for(int spalte = 0; spalte < cells[0].length; spalte++)
            {
                cells[reihe][spalte].propagate();
            }
        }
    }

    /**
     * zählt die lebenden Nachbarn der Zelle
     */
    private int countNeighbours(int r, int s)
    {
        int count = 0;

        int countReihen = cells.length;
        int countSpalten = cells[0].length;

        // für ein 3x3 Feld
        for(int reihe = r - 1; reihe <= r + 1; reihe++)
        {
            for(int spalte = s - 1; spalte <= s + 1; spalte++)
            {
                //die Modulo-Operation läßt an den Bildschirmrändern jeweils weiterzählen
                //die Zählung wird dann auf der anderen Seite des Bildschirms durchgeführt
                if(cells[(reihe+countReihen)%countReihen][(spalte+countSpalten)%countSpalten].isAlive())
                {
                    count++;
                }
            }
        }

        //weil die aktuelle Zelle mitgezählt wurde
        if(cells[r][s].isAlive())
        {
            count--;
        }

        return count;
    }

    //liefert die Anzahl der Spalten des Organismus
    public int getOrganismWidth()
    {
        return cells[0].length;
    }

    //liefert die Anzahl der Reihen des Organismus
    public int getOrganismHeight()
    {
        return cells.length;
    }

    //liefert die Aussage, ob der Organismus an der Stelle (r,s) eine lebende Zelle hat oder nicht
    public boolean isAlive(int r, int s)
    {
      return cells[r][s].isAlive();
    }

    //lädt die Konfiguration des Organismus aus einer Datei
    public void loadFromFile(String f)
    {
        //initialisiere einen Organismus mit ausschließlich toten Zellen, um ein "leeres" Feld zu erzeugen
        createGeneration(DEAD);

        //positioniere das aus der Datei geladene Muster circa in der Mitte des Organismus
        int colStart = cells[0].length / 2;
        int rowStart = cells.length / 2;

        int row = rowStart;
        int col=colStart;

        //Datei auslesen
        Path p = Paths.get(f);

        if(Files.isRegularFile(p))
        {
            try(FileReader inputStream = new FileReader(f))
            {
                int c;
                while((c = inputStream.read()) != -1)
                {
                    switch(c)
                    {
                        //in der Datei steht ein . für eine tote Zelle
                        case '.':
                            cells[row][col++] = new Cell(DEAD);
                            break;
                        //in der Datei steht das O für eine lebende Zelle
                        case 'O':
                            cells[row][col++] = new Cell(ALIVE);
                            break;

                        //wird der Zeilenumbruch gelesen, dann geht es im Organismus in der nächsten Reihe weiter
                        case '\n':
                            row++;
                            col=colStart;
                            break;
                    }
                }
            } catch(IOException e)
            {

            }
        }

    }
}
