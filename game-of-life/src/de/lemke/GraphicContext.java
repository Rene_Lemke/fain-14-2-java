package de.lemke;

import java.awt.*;

public class GraphicContext
{
    //ermitteln der grafischen Umgebung
    private GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
    //speichern der Ausgabegeräte (Monitor) in ein Array
    private GraphicsDevice[] gd = ge.getScreenDevices();
    //das Gerät mit dem Index sollte es immer geben
    private GraphicsDevice currentGraphicDevice = gd[0];

    //die Anzahl der Bildschirme
    private int numberScreens;

    //die Bildschirmmaße
    private int width;
    private int height;

    /**
     * der Konstruktor ermittelt für den aktuellen Monitor (s.Zeile 12) die Abmessungen und
     * ermittelt die Anzahl der verfügbaren Monitore insgesamt
     */
    public GraphicContext()
    {
        //Abmessung des aktuellen Monitors
        width = currentGraphicDevice.getDefaultConfiguration().getBounds().width;
        height = currentGraphicDevice.getDefaultConfiguration().getBounds().height;

        //Anzahl der verfügbaren Monitore
        numberScreens = gd.length;
    }

    //liefert die aktuelle Breite des Bildschirms
    public int getCurrentWidth()
    {
        return width;
    }

    //liefert die aktuelle Höhe des Bildschirms
    public int getCurrentHeight()
    {
        return height;
    }

    //liefert den aktuell verwendeten Monitor
    public GraphicsDevice getCurrentGraphicDevice()
    {
        return currentGraphicDevice;
    }

    //schaltet um auf den Monitor mit der Nummer n (Nummerierung beginnt bei 0)
    public void switchToScreen(int n)
    {
     //die Zusicherung wirft eine Exception, falls n nicht kleiner als die Anzahl der Monitore ist
     assert ( n > numberScreens);

     //Umschalten auf den neuen Monitor
     currentGraphicDevice = gd[n];

     //...und ermitteln der u.U. abweichenden Bildschirmdimensionen
     width = currentGraphicDevice.getDefaultConfiguration().getBounds().width;
     height = currentGraphicDevice.getDefaultConfiguration().getBounds().height;
    }
}
