package de.lemke;

import javax.swing.*;
import java.awt.*;
import java.awt.geom.Rectangle2D;

public class PaintArea extends JPanel implements Runnable
{
    //Limits derZellengrößen
    private final int MIN_CELL_SIZE = 1;
    private final int MAX_CELL_SIZE = 10;

    //der Abstand zwischen den Zellen in der Ausgabe
    private int step = 1;

    //die Größe einer Zelle in der grafischen Ausgabe
    private int dim = 4;

    //die Pause zwischen den Generationen (hier 600ms)
    private final int PAUSE = 600;

    //der grafische Context wird injiziert
    private GraphicContext gc;

    //der Organismus wird zur Berechnung der Zellen und jetzt dann auch zum Zeichnen der
    //Rechtecke benötigt
    private Organism organism;

    //die Steuerung des Threads erfolgt über eine boolesche Variable
    //true = Thread läuft, false = Thread läuft nicht
    private boolean animRunning = false;

    /**
     * Konstruktor
     */
    public PaintArea(GraphicContext context)
    {
        //der grafische Kontext wird injiziert
        gc = context;

        //der neue Organismus wird erzeugt, die Werte für den grafischen Context,
        //die Größe der Zelle und der Abstand der Zellen wird übergeben
        organism = new Organism(context, dim, step);

        //das JPanel wird so groß wie die verfügbare Fensterfläche
        setPreferredSize(new Dimension(gc.getCurrentWidth(), gc.getCurrentHeight()));

        //die Hintergrundfarbe ist die Farbe der toten Zellen (cyan)
        setBackground(Color.CYAN);

        //JPanel-Änderungen validieren und zeichnen lassen
        validate();
    }

    @Override
    public void paint(Graphics g)
    {
        //paintComponent der Basisklasse
        super.paintComponent(g);

        //Umschalten auf 2D-Grafik
        Graphics2D g2 = (Graphics2D) g;

        //vom Organismus die Anzahl der Reihen und Spalten holen
        int maxReihe = organism.getOrganismHeight();
        int maxSpalte = organism.getOrganismWidth();

        //für jede Zelle des Organismus wird jetzt ein Rechteck gezeichnet
        for(int reihe = 0; reihe < maxReihe; reihe++)
        {
            for(int spalte = 0; spalte < maxSpalte; spalte++)
            {
                //wenn die Zelle lebt...
                if(organism.isAlive(reihe,spalte))
                {
                    g.setColor(Color.BLACK);             //dann ein schwarzes Rechteck
                }
                else
                {
                    g.setColor(Color.CYAN);             //ansonsten hat das Rechteck die Hintergrundfarbe
                }

                //hier wird das Rechteck gezeichnet (xpos, ypos, breite,höhe) - mit etwas Abstand zwischen den Rechtecken
                g2.fill(new Rectangle2D.Double(spalte * (dim + step), reihe * (dim + step), dim, dim));
            }
        }
    }

    //Animation soll stoppen
    public void stopAnim()
    {
        animRunning = false;
    }


    /**
     * die Methode wird von Thread.start() aufgerufen, um die Logik als eigenständigen Thread
     * laufen zu lassen.
     */
    @Override
    public void run()
    {
        //Animation läuft...
        animRunning = true;

        //kurze Pause (um den Anfangszustand sehen zu können)
        makePause();

        //solange die Animation läuft (drückt jmd. ESC oder Q, dann endet die Schleife)
        while(animRunning)
        {
            //berechne die nächste Generation
            organism.nextGeneration();
            //...und lasse die Ausgabe neu zeichnen
            repaint();
            //kurze Pause, damit die Ausgabe beobachtet werden kann
            makePause();
        }
    }

    /**
     * Pause machen .. der Wert für die Pause lässt sich einstellen (Zeile 20)
     */
    private void makePause()
    {
        try
        {
            Thread.sleep(PAUSE);
        }
        catch(InterruptedException e)
        {
            e.printStackTrace();
        }
    }

}
