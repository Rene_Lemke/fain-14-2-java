package de.lemke;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

/**
 * Die GUI stellt ein Fenster bereit, das auf die Tasten ESC und Q reagiert
 */

public class Tut01Gui extends JFrame implements KeyListener
{
    //ein grafisches Context-Objekt erzeugen
    private GraphicContext gc = new GraphicContext();

    //die Animation läuft als eigenständiger Thread
    private Thread animThread;

    //die eigentliche Zeichenfläche
    private PaintArea paintArea;

    /**
     * Konstruktor
     */
    public Tut01Gui()
    {
        //ruft den Konstruktor der Basisklasse auf
        super();

        //trägt sich als KeyListener ein(reagiert also auf Tasten)
        addKeyListener(this);

        //hier kann auf einen anderen Monitor umgeschaltet werden
        //gc.switchToScreen(1);  // 0...primary screen, 1...secondary screen

        //legt die Größe fest
        setPreferredSize(new Dimension(gc.getCurrentWidth(), gc.getCurrentHeight()));

        //hier wird die Fensterdekoration entfernt, es gibt also keinen Rahmen, keine Titelleiste,
        //keine Schaltflächen für die Fenstersteuerung
        setUndecorated(true);

        //das Fenster darf in seiner Größe nicht verändert werden
        setResizable(false);

        //stellt das Fenster im Vollbildmodus auf dem aktuell verwendeten Monitor dar
        gc.getCurrentGraphicDevice().setFullScreenWindow(this);

        //hier werden die Komponenten nachgeladen (PaintArea)
        initComponents();

        //der JFrame wird auf seine Komponenten angepasst bzgl. der Größe
        pack();

        //der JFrame wird sichtbar gemacht
        setVisible(true);

        //Änderungen am JFrame werden validiert = gezeichnet
        validate();
    }

    /**
     * erstellt die Komponenten, die zum JFrame (also zur GUI) dazugehören sollen
     */
    private void initComponents()
    {
        //die Zeichenfläche (in orange)
        paintArea = new PaintArea(gc);

        //wird zum JFrame hinzugefügt
        add(paintArea);
    }

    /**
     * Starten der Simulation
     * die PaintArea implementiert Runable und läßt sich als eigenständiger Thread
     * (parallel zum MainThread) ausführen
     *
     * Threads werden via start()-Methode gestartet, sie ruft dann für den Thread
     * die Methode run() auf
     */
    public void startAnim()
    {
        //einen neuen Thread erzeugen und in diesen das Runable-Objekt "PaintArea" injizieren
        animThread = new Thread(paintArea);
        //den Thread starten
        animThread.start();
    }

    /**
     * die Methoden, die das Interface KeyListener verlangt
     *
     */
    @Override
    public void keyTyped(KeyEvent e)
    {
        //würde aufgerufen werden, wenn das Unicode-Zeichen des Keyboards zum System-Input gesendet wird
    }

    @Override
    public void keyPressed(KeyEvent e)
    {
        System.out.format("%s%n", e);

        switch(e.getKeyCode())
        {
            //falls Escape oder Q gedrückt wurde
            case KeyEvent.VK_ESCAPE:
            case KeyEvent.VK_Q:
                //...Animation anhalten
                paintArea.stopAnim();
                //...belegten "Grafikspeicher" des JFrames freigeben
                dispose();
                break;
        }
    }

    @Override
    public void keyReleased(KeyEvent e)
    {

    }
}
