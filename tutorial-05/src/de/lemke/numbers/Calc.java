package de.lemke.numbers;

class Calc
{
 public static void main(String [] args)
   {
    int a = Integer.parseInt(args[0]);
    int b = Integer.parseInt(args[2]);

    int ergebnis = 0;

    if(args[1].equals("+")) ergebnis = a+b;
    if(args[1].equals("-")) ergebnis = a-b;
    if(args[1].equals("*")) ergebnis = a*b;
    if(args[1].equals("/")) ergebnis = a/b;
    if(args[1].equals("%")) ergebnis = a%b;

    System.out.format("Ergebnis als Dezimalzahl : " + a + args[1] + b + " = " + Integer.toString(ergebnis,10));
    System.out.format("%nErgebnis als Hexadezimalzahl : " + "%1$X" + args[1] + "%2$X" + " = " + "%3$X" + "%n", a, b, ergebnis);
   }
}
