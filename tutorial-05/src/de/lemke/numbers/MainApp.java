package de.lemke.numbers;

import java.util.Calendar;                           // für Zeitangaben, Calendar
import java.util.Locale;                             // wird benötigt für Locale.GERMANY

public class MainApp
{
  public static void main( String [] args)
     {

      //Konstruktor mit int-Wert und String als Parameter (überladen)
      Integer zahlA = new Integer(-1000);
      Integer zahlB = new Integer("4321");

      System.out.println(zahlA + " , " + zahlB);

      //Zahl aus Zeichenkette dekodieren lassen
      Integer hexA = Integer.decode("0xA0");      //Integerzahl wird aus einer Hexadezimalzahl berechnet
      Integer hexB = Integer.decode("0XEF");
      Integer hexC = Integer.decode("#FF");

      //dasselbe für Dekodierung Integer -> Oktalzahl
      Integer octA = Integer.decode("010");

      System.out.println("0xA0, 0XEF, #FF, 010 .. " + hexA + " , " + hexB + " , " + hexC + " , " + octA);

  /*NumberFormatException
      Integer nfe = Integer.decode("YES");
  */
      //Zahl aus einer Zeichenkette ermitteln. Die Basis (radix) wird als 2.Parameter angegeben.
      Integer binA = Integer.valueOf("1001",2);
      Integer octB = Integer.valueOf("1001",8);
      Integer decC = Integer.valueOf("1001",10);
      Integer hexD = Integer.valueOf("1001",16);

      System.out.println("1001 .. " + binA + " , " +octB+ " , " +decC+ " , " + hexD);

      //wir geben eine Integerzahl als String aus
      int x = 100;

      System.out.println(x + " : " + Integer.toBinaryString(x) + " (als Binär) " );
      System.out.println(x + " : " + Integer.toOctalString(x) + " (als Oktal) ");
      System.out.println(x + " : " + Integer.toHexString(x) + " (als Hexadezimal)");

      System.out.println(x + " : " + Integer.toString(x,2) );
      System.out.println(x + " : " + Integer.toString(x,8) );
      System.out.println(x + " : " + Integer.toString(x,10) );
      System.out.println(x + " : " + Integer.toString(x,16) );

      //Ausgaben formatieren
      System.out.format("%n ---Ausgabeformatierungen--- %n");

      long zahl = 543321;

      System.out.format("Zahl: %d %n", zahl);      // %n = Zeilenumbruch da kein println
      System.out.format("Zahl: %08d %n", zahl);    // 8 Stellen der Zahl werden angezeigt, ggf mit Nullen aufgefüllt
      System.out.format("Zahl: %-8d %n", zahl);     // 8 Stellen der Zahl werden angezeigt, ohne Auffüllung (-8d für linksbündige Ausgabe, 8d für rechtsbündig)
      System.out.format("Zahl: %8d %n", zahl);

      double pi = Math.PI;

      System.out.format("%nDie Kreiszahl Pi: %f %n", pi);
      System.out.format("Die Kreiszahl Pi: %.3f %n", pi);
      System.out.format("Die Kreiszahl Pi: %10.3f %n", pi);   // 10 Vorkommastellen, 3 Nachkommastellen
      System.out.format("Die Kreiszahl Pi: %-10.3f %n", pi);
      System.out.format(Locale.GERMANY, "Die Kreiszahl Pi in deutscher Formatierung: %-10.3f %n", pi);
      System.out.format(Locale.US, "Die Kreiszahl Pi in amerikanischer Formatierung: %-10.3f %n", pi);

      //Zeitangaben
      Calendar cal = Calendar.getInstance();

      System.out.format("%n ---Zeitangaben--- %n");
      System.out.format("%tB %te %tY %n", cal,cal,cal);      //Datumsausgabe in Monat (als Name),Tag, Jahr
      System.out.format(Locale.CHINESE, "%tB %te %tY %n", cal,cal,cal);   //selbiges Datum in Chinesisch

      //explizit die Indizierung der Werte
      System.out.format("%n ---explizite Indizierung von Werten--- %n");
      System.out.format(Locale.US, "%1$tB %1$te %1$tY %n", cal);     // das 1$ bezieht sich auf den ersten Wert der Formatliste, hier: cal

      System.out.format("%3$d %1$d %2$d %n", hexA, hexB, hexC);        // zuerst wird hexC, dann hexA und dann hexB ausgegeben
     }
}
