package de.lemke.numbers;

public class AltCalc
{
 public static void main ( String [] args )
        {
          if( args.length != 3 )
              {
                System.out.println("Verwendung des Rechners mit AltCalc <Zahl1> <Operator> <Zahl2>");
                return;
              }
          Integer zahlA = Integer.decode(args[0]);
          Integer zahlB = Integer.decode(args[2]);

          char op = args[1].charAt(0);

          Integer ergebnis = new Integer(0);

          switch(op)
            {
              case '+':
                 ergebnis = zahlA + zahlB;
                 break;
              case '-':
                 ergebnis = zahlA - zahlB;
                 break;
              case '*':
                 ergebnis = zahlA * zahlB;
                 break;
              case '/':
                 ergebnis = zahlA / zahlB;
                 break;
              case '%':
                ergebnis = zahlA % zahlB;
                break;
            }
          System.out.format("Als Dezimalzahlen: %d %c %d = %d %n", zahlA, op, zahlB, ergebnis);
          System.out.format("Als Hexadezimalzahlen: %02X %c %02X = %02X %n", zahlA, op, zahlB, ergebnis);

        }  
}
