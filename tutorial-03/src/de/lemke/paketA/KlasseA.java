package de.lemke.paketA;

public class KlasseA
{

  public boolean bloedeIdee = true;
  protected boolean bessereIdee = true;
  private boolean besteIdee = true;

  //ohne Modifizierer = package level
  void sendMessage()
     {
       System.out.println("Hallo Welt!");
     }
}
