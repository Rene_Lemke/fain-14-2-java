package de.lemke.paketB;
import de.lemke.paketA.*;

public class KlasseD
{
  public void schauenWirMal()
    {
      KlasseA objA = new KlasseA();
      System.out.println(objA.bloedeIdee);
      System.out.println(objA.bessereIdee);
      System.out.println(objA.besteIdee);

      objA.sendMessage();
    }
}
