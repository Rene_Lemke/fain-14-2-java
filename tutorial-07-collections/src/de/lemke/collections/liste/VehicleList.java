package de.lemke.collections.liste;

import java.util.*;
import de.lemke.collections.vehicle.*;

public class VehicleList extends ArrayList<Vehicle>
{
  public VehicleList getAllReadyToUse()
    {
     VehicleList vl = new VehicleList();

     for(Vehicle v: this)
       {
        if(v.getCurrentState() == Status.FERTIG)
          {
           vl.add(v);
          }
       }
      return vl;
    }

  public VehicleList getCarReadyToUse()
    {
     VehicleList vl = new VehicleList();

     for(Vehicle v: this)
       {
        if(v.getCurrentState() == Status.FERTIG && v instanceof ACar)
          {
           vl.add(v);
          }
       }
      return vl;
    }

  public VehicleList getLKWReadyToUse()
   {
    VehicleList vl = new VehicleList();

    for(Vehicle v: this)
      {
       if(v.getCurrentState() == Status.FERTIG && v instanceof ALKW)
         {
          vl.add(v);
         }
      }
    return vl;
   }

  @Override
  public String toString()
    {
     StringBuilder sb = new StringBuilder();

     sb.append(String.format("%22s %10s %14s%n", "Typ", "km-Stand", "Status"));
     sb.append(String.format("-------------------------------------------------%n"));

     for(Vehicle v: this)
       {
        sb.append(v);
        sb.append(String.format("%n"));
       }
     return sb.toString();
    }
}
