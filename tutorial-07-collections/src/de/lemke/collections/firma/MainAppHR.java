package de.lemke.collections.firma;

import java.util.*;

public class MainAppHR
{
  private static final String RESET = "\u001B[0m";
  private static final String ROT = "\u001B[31m";
  public static final String GRUEN = "\u001B[32m";
  public static final String GELB = "\u001B[33m";
  public static final String BLAU = "\u001B[34m";
  public static final String CYAN = "\u001B[36m";

  public static void main(String [] args)
   {
    Angestellter a1 = new Angestellter("Lenning", "Rennus");
    a1.einstellen(27,10,2014);
    a1.setGehalt(2000);

    Angestellter a2 = new Angestellter("Strichel", "Pascala");
    a2.einstellen(28,11,2014);
    a2.setGehalt(1800);

    Angestellter a3 = new Angestellter("Tigris", "Michael");
    a3.einstellen(19,12,2014);
    a3.setGehalt(1600);

    AngListe al = new AngListe();

    al.add(a1);
    al.add(a2);
    al.add(a3);

    System.out.println(al);

    Collections.sort(al, Angestellter.NAME);
    System.out.println(al);

    Collections.sort(al, Angestellter.VORNAME);
    System.out.println(al);

    Collections.sort(al, Angestellter.GEHALT);
    System.out.println(al);

    Collections.sort(al, Angestellter.EINDATUM);
    System.out.println(al);

    Collections.sort(al, Angestellter.ENTDATUM);
    System.out.println(al);
   }
}
