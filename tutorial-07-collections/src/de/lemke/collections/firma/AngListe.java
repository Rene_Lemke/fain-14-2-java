package  de.lemke.collections.firma;

import java.util.*;

public class AngListe extends ArrayList<Angestellter> {

  @Override
  public String toString() {

    StringBuilder sb = new StringBuilder();

    sb.append(String.format("%s%n","Die Angestellten: "));

    for(Angestellter a: this) {

      sb.append(String.format("%s%n", a));
    }

    return sb.toString();
  }
}
