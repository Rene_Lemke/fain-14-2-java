package de.lemke.collections.firma;

import java.util.*;
import java.text.SimpleDateFormat;

public class Angestellter
{
  protected String name;
  protected String vorname;
  protected Integer gehalt;
  protected Calendar einstellung = Calendar.getInstance();
  protected Calendar entlassung = Calendar.getInstance();

  private boolean entlassen = false;

  //Standardkonstruktor
  public Angestellter()
    {
      this.name = "";
      this.vorname = "";
      this.gehalt = 0;
    }

    public Angestellter(String n, String v)
     {
      this.name = n;
      this.vorname = v;
      this.gehalt = 1000;
     }

  //Konstruktor welcher das dritte Argument von String nach Datum konvertiert
  public Angestellter(String n, String v, String e)
   {
    this.name = n;
    this.vorname = v;
    this.gehalt = 1000;

    //Datum erzeugen
    String [] parts = e.split("\\.");
    this.einstellung.set(Calendar.YEAR, Integer.parseInt(parts[2]));
    this.einstellung.set(Calendar.MONTH, Integer.parseInt(parts[1])-1);
    this.einstellung.set(Calendar.DAY_OF_MONTH, Integer.parseInt(parts[0]));
   }

  public void setGehalt(int g)
   {
    this.gehalt = g;
   }

  //Datum der Einstellung
  public void einstellen(int tag, int monat, int jahr)
    {
      this.einstellung.set(Calendar.YEAR, jahr);
      this.einstellung.set(Calendar.MONTH, monat-1);
      this.einstellung.set(Calendar.DAY_OF_MONTH, tag);
    }

  //Datum der Entlassung
   public void entlassen(int tag, int monat, int jahr)
    {
      this.entlassen = true;

      this.entlassung.set(Calendar.YEAR, jahr);
      this.entlassung.set(Calendar.MONTH, monat-1);
      this.entlassung.set(Calendar.DAY_OF_MONTH, tag);
    }

   /*
   * Comparatoren
   */
   //Comparator nach Name
   public static Comparator<Angestellter> NAME = new Comparator<Angestellter>()
     {
      @Override
      public int compare(Angestellter a, Angestellter b)
        {
          return a.name.compareTo(b.name);
        }
     };

     //Comparator nach Vorname
     public static Comparator<Angestellter> VORNAME = new Comparator<Angestellter>()
       {
        @Override
        public int compare(Angestellter a, Angestellter b)
          {
            return a.vorname.compareTo(b.vorname);
          }
       };

       //Comparator nach Gehalt
       public static Comparator<Angestellter> GEHALT = new Comparator<Angestellter>()
         {
          @Override
          public int compare(Angestellter a, Angestellter b)
            {
              return a.gehalt.compareTo(b.gehalt);
            }
         };

      //Comparator nach Einstellungsdatum
     public static Comparator<Angestellter> EINDATUM = new Comparator<Angestellter>()
         {
          @Override
          public int compare(Angestellter a, Angestellter b)
            {
              return a.einstellung.compareTo(b.einstellung);
            }
         };

     //Comparator nach Entlassungsdatum
     public static Comparator<Angestellter> ENTDATUM = new Comparator<Angestellter>()
       {
        @Override
        public int compare(Angestellter a, Angestellter b)
          {
            return a.entlassung.compareTo(b.entlassung);
          }
       };

   //Überschreiben der toString()-Methode
   @Override
   public String toString()
     {
       SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");

       StringBuilder sb = new StringBuilder();

       sb.append(String.format("%s %s | %d | %s", this.name, this.vorname, this.gehalt, dateFormat.format(this.einstellung.getTime())));

       if(this.entlassen)
         {
           sb.append(String.format("%s", dateFormat.format(this.entlassung.getTime())));
         }
      else
         {
          sb.append(String.format("%s", "  -  "));
         }
      return sb.toString();
     }
}
