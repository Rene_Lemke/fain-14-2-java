package de.lemke.collections;

import java.util.*;
import java.util.concurrent.*;                 //für ArrayBlockingQueue

public class MainApp
{
  private static final String RESET = "\u001B[0m";
  private static final String ROT = "\u001B[31m";
  public static final String GRUEN = "\u001B[32m";
  public static final String GELB = "\u001B[33m";
  public static final String BLAU = "\u001B[34m";
  public static final String CYAN = "\u001B[36m";

  public static void main(String [] args)
   {
    List<String> liste = new ArrayList<String>();    //erzeugt eine neue leere ArrayList

    liste.add("Hallo");
    liste.add(new String("Welt!"));
    liste.add("eins");
    liste.add("eins");

    List<String> andereListe = new ArrayList<String>(liste);   // erzeugt eine neue Liste mit den Elementen der vorhandenen Liste

    List<String> nochEineListe = new ArrayList<String>(1000);  // erzeugt eine neue Liste mit der angegebenen Kapazität

    System.out.format("list[%d],andereListe[%d],nochEineListe[%d]%n", liste.size(), andereListe.size(), nochEineListe.size());

    andereListe.add("wir");
    andereListe.add("brauchen");
    andereListe.add("eine Main-Methode");

    System.out.format("Die Liste ist in der andereListe enthalten: " + ROT + " %b %n" + RESET, andereListe.containsAll(liste));

    for(String s: andereListe)
     {
       System.out.println(s);
     }

    String tmp = andereListe.remove(2);               // entfernt ein Element aus der Liste an Position #2

    System.out.println("------------");

    for(String s: andereListe)
     {
       System.out.println(s);
     }

    System.out.println("Entfernt wurde: " + ROT + tmp + RESET);


    /**
    * Interface Set
    */
    System.out.format("%n------Interface Set-------%n");
    Car a1 = new Car();
    Car a2 = new Car();

    System.out.format("a1==a2 (%b)%n", a1.equals(a2));
    System.out.format("a1==a2(%b)%n", a1.hashCode() == a2.hashCode());

    Car a3 = new Car(100, "blau", 120, "Opel");

    System.out.format("a1==a3 (%b)%n", a1.equals(a3));
    System.out.format("a1==a3 (%b)%n", a1.hashCode() == a3.hashCode());

    Set<Car> carSet = new TreeSet<Car>();

    carSet.add(a1);
    carSet.add(a2);
    carSet.add(a3);

    System.out.format("carSet[%d] %n", carSet.size());

    /**
    * Interface List
    *
    */
    System.out.format("%n------Interface List-------%n");
    List<Integer> intListe = new ArrayList<Integer>();

    //Elemente hinzufügen (auch Duplikate)

    intListe.add(10);
    intListe.add(10);
    intListe.add(10);
    intListe.add(10);

    intListe.add(20);
    intListe.add(20);
    intListe.add(20);

    intListe.add(30);
    intListe.add(30);
    intListe.add(30);
    intListe.add(30);

    System.out.println(intListe);
    System.out.format("Das Objekt 10 befindet sich an Stelle %d. %n", intListe.indexOf(10));
    System.out.format("Das Objekt 10 kommt zum letzten Mal an der Stelle %d vor. %n", intListe.lastIndexOf(10));

    System.out.println("Das ist eine Subliste von intListe: " + intListe.subList(4,8));      //SubListe: Index 4 - 8 der Liste


  /**
  * Interface Queue
  *
  */
  System.out.format("%n------Interface Queue-------%n");

  Queue<Integer> queue = new LinkedList<Integer>();

  for(int i=10; i >= 0; i -= 1 )
    {
      queue.add(i);
    }

  System.out.println(queue);

  System.out.println("Das erste Element der queue ist: " + queue.peek());
  while(!queue.isEmpty())
    {
      System.out.println(queue.remove());        //holt das erste Element aus der Warteschlange
      System.out.println("Das erste Element der queue ist: " + queue.peek());       // peek statt element benutzen um die NoSuchElementException zu umgehen sobald die Queue leer(null) ist
    }

   Queue<Integer> arrayQueue = new ArrayBlockingQueue<Integer>(10);

   for(int i=0; i<11; i++)
     {
      System.out.format("%s: %b %n", arrayQueue, arrayQueue.offer(i));    // gibt die Queue aus und TRUE | FALSE der offer()-Operation
    }                                                                    // offer(i) ist die sicherere Variante als add(i)
  //die Warteschlange ist voll(10), weiteres Hinzufügen führt zu einer IllegalStateException: Queue full

  /**
  * Interface Dequeue
  *
  */

   System.out.format("%n------Interface Dequeue-------%n");

   Deque<Integer> deque = new ArrayDeque<Integer>(10);                    // eine double ended queue mit 10 Elementen

   System.out.format("Deque: %s mit %d Anzahl Elementen. %n", deque, deque.size());

   deque.offerFirst(10);
   deque.offerFirst(20);
   deque.offerLast(30);
   deque.offerLast(40);

   System.out.format("Deque: %s mit %d Anzahl Elementen. %n", deque, deque.size());

   deque.addFirst(50);
   deque.addFirst(60);
   deque.addFirst(70);
   deque.addFirst(80);

   deque.addLast(90);
   deque.addLast(100);

   System.out.format("Deque: %s mit %d Anzahl Elementen. %n", deque, deque.size());

   deque.addLast(110);      // die Startgröße von 10 wird hier überschritten, das ArrayDeque wächst automatisch mit

   System.out.format("Deque: %s mit %d Anzahl Elementen. %n", deque, deque.size());

   /**
   * Interface Map
   *
   */

    System.out.format("%n------Interface Map-------%n");

    Map<Integer,String> map = new HashMap<Integer,String>();                    // bzw new Map<>();

    map.put(1,"Hallo");
    map.put(2,"Welt!");
    map.put(3, "Wir reden hier über das Leitungswasser");
    map.put(3, "Mal schauen, ob das geht!");                                    // vorhergehene Zeile mit key=3 wird überschrieben

    System.out.println(map);

    //Key set
    Set<Integer> keys = map.keySet();                     // keySet extends AbstractSet und dieses implementiert Set
    Collection<String> values = map.values();

    System.out.println("keys ist ein: " + keys.getClass());
    System.out.println("values ist ein: " + values.getClass());
    System.out.println("Schlüssel: " + keys);
    System.out.println("Werte: " + values);

    System.out.format("Der Schlüssel 3 ist in der Map enthalten: " + ROT + "%b%n" + RESET, map.containsKey(3));
    System.out.format("Der Schlüssel 4 ist in der Map enthalten: " + ROT + "%b%n" + RESET, map.containsKey(4));

    System.out.format("Der Wert \"Hallo\" ist in der Map enthalten: " + ROT + "%b%n" + RESET, map.containsValue("Hallo"));
    System.out.format("Der Wert \"Michaelas Muttiheft\" ist in der Map enthalten: " + ROT + "%b%n" + RESET, map.containsValue("Michaelas Muttiheft"));

    /**
    * Arbeiten mit Comparator
    */
    System.out.format("%n------Comparator-------%n");

    List<CarSort> lcs = new ArrayList<CarSort>();

    lcs.add(new CarSort(1000, "rot", 120, "Toyota"));
    lcs.add(new CarSort(2000, "blau", 100, "Seat"));
    lcs.add(new CarSort(30000, "silber", 220, "Mercedes"));
    lcs.add(new CarSort(4000, "grau", 80, "Opel"));
    lcs.add(new CarSort(500, "grün", 60, "Subaru"));
    lcs.add(new CarSort(10, "gelb", 320, "Twingo"));

    System.out.println(lcs);

    // wir lassen mit dem Comparator sortieren
    Collections.sort(lcs, new CarSortCmpFarbe());                                // ...nach der Farbe sortieren lassen
    System.out.println(lcs);

    Collections.sort(lcs, new CarSortCmpHersteller());                                // ...nach dem Hersteller sortieren lassen
    System.out.println(lcs);

    //das ganze nochmal mit anonymen Klassen in Car
    // sortieren nach Farbe
/*    Collections.sort(lcs, Car.COMP_FARBE);
    System.out.println(lcs);

    // sortieren nach Leistung
    Collections.sort(lcs, Car.COMP_LEISTUNG);
    System.out.println(lcs);

    // sortieren nach Hersteller
    Collections.sort(lcs, Car.COMP_HERSTELLER);
    System.out.println(lcs);

    // sortieren nach Kilometerstand
    Collections.sort(lcs, Car.COMP_KM);
    System.out.println(lcs);
*/

   /*
   * Algorithmen auf Collections
   */
   System.out.format("%n------Algorithmen auf Collections-------%n");

   List<Integer> integerList = new LinkedList<Integer>();
   integerList.add(0);
   integerList.add(-12);
   integerList.add(20);
   integerList.add(-100);
   integerList.add(75);

   System.out.println("unsortiert: " + integerList);

   Collections.sort(integerList, null);                      //LinkedList hat bereits einen eigenen Comparator
   System.out.println("sortiert: " + integerList);

   Collections.reverse(integerList);
   System.out.println("umgekehrt sortiert: " + integerList);

   Collections.shuffle(integerList);
   System.out.println("gemischt: " + integerList);

   Collections.rotate(integerList, 2);
   System.out.println("rotiert: " + integerList);

   System.out.format("max: %d, min: %d %n", Collections.max(integerList), Collections.min(integerList));

   //erst Liste sortieren, dann suchen
   Collections.sort(integerList, null);
   System.out.format("Die 20 steht an der Position: %d %n", Collections.binarySearch(integerList,20));

   Collections.fill(integerList, 1000);
   System.out.println(integerList);
  }
}
