package de.lemke.collections;

import java.util.*;

public class Car implements Comparable<Car>
{
 private static int id=1;              //eindeutiger Identifizierer(Schlüssel) der Fahrzeuge

 private int nummer;

 protected Integer kilometerstand;
 protected String farbe;
 protected Integer motorleistung;
 protected String hersteller;

 public Car()
   {
     this.nummer = id++;
     this.kilometerstand = 0;
     this.farbe = "rot";
     this.motorleistung = 50;
     this.hersteller = "VW";
   }

 public Car(int km, String f, int mo, String he)
   {
     this.nummer = id++;
     this.kilometerstand = km;
     this.farbe = f;
     this.motorleistung = mo;
     this.hersteller = he;
   }

 public int getNummer()
   {
    return this.nummer;
   }

 public int getKilometerstand()
   {
     return this.kilometerstand;
   }

 public int getMotorleistung()
   {
     return this.motorleistung;
   }

public String getFarbe()
  {
    return this.farbe;
  }

public String getHersteller()
  {
    return this.hersteller;
  }

@Override
public boolean equals(Object o)
  {
    if(this.getClass() != o.getClass())
        return false;
    if(this == o)
        return true;

    Car tmp = (Car)o;

    return this.nummer              == tmp.nummer;

/*         this.getKilometerstand() == tmp.getKilometerstand()
        && this.getMotorleistung()  == tmp.getMotorleistung()
        && this.getFarbe().equals(tmp.getFarbe())
        && this.getHersteller().equals(tmp.getHersteller());
*/
  }

@Override
public int hashCode()
  {
    int hash = 7;
    int multi = 42;

    hash = hash * multi + nummer;
/*    hash = hash * multi + kilometerstand;                |
    hash = hash * multi + motorleistung;                   > fallen weg, da es immer konstistent sein muss mit Methode equals!
    hash = hash * multi + this.farbe.hashCode();           |
    hash = hash * multi + this.hersteller.hashCode();      |
*/
    return hash;
  }

@Override
public int compareTo(Car c)
  {
    return this.nummer - c.nummer;
  }


  /*
  *   Die Comparatoren als anonyme Klassen innerhalb der Klasse, für die sie geschrieben worden sind
  */
   //der Comparator für die Farbe
  protected static Comparator<Car> COMP_FARBE = new Comparator<Car>()
    {
      public int compare(Car c1, Car c2)
        {
          return c1.farbe.compareTo(c2.farbe);
        }
    };

  //der Comparator für die Leistung
  protected static Comparator<Car> COMP_LEISTUNG = new Comparator<Car>()
    {
      public int compare(Car c1, Car c2)
        {
          return c1.motorleistung.compareTo(c2.motorleistung);
        }
    };

    //der Comparator für den Kilometerstand
    protected static Comparator<Car> COMP_KM = new Comparator<Car>()
      {
        public int compare(Car c1, Car c2)
          {
            return c1.kilometerstand.compareTo(c2.kilometerstand);
          }
      };

   //der Comparator für die Hersteller
    protected static Comparator<Car> COMP_HERSTELLER = new Comparator<Car>()
      {
        public int compare(Car c1, Car c2)
          {
            return c1.hersteller.compareTo(c2.hersteller);
          }
      };
}
