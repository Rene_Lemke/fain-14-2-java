package de.lemke.collections.vehicle;

import de.lemke.collections.vehicle.*;
import de.lemke.collections.liste.*;

public class MainApp {


	public static void main(String [] args) {

		VehicleList vl = new VehicleList();

		vl.add(new Car(0,Status.FERTIG));
		vl.add(new Car(1,Status.FERTIG));
		vl.add(new Car(2,Status.FERTIG));
		vl.add(new Car(3,Status.FERTIG));
		vl.add(new Car(4,Status.FERTIG));
		vl.add(new Car(5,Status.WARTUNG));
		vl.add(new Car(6,Status.WARTUNG));
		vl.add(new Car(7,Status.WARTUNG));
		vl.add(new Car(8,Status.WARTUNG));
		vl.add(new Car(9,Status.WARTUNG));

		vl.add(new LKW(0,Status.FERTIG));
		vl.add(new LKW(1,Status.FERTIG));
		vl.add(new LKW(2,Status.FERTIG));
		vl.add(new LKW(3,Status.FERTIG));
		vl.add(new LKW(4,Status.FERTIG));
		vl.add(new LKW(5,Status.WARTUNG));
		vl.add(new LKW(6,Status.WARTUNG));
		vl.add(new LKW(7,Status.WARTUNG));
		vl.add(new LKW(8,Status.WARTUNG));
		vl.add(new LKW(9,Status.WARTUNG));

		// alle Vehicle ausgeben
		System.out.println(vl);

		VehicleList readyCar = vl.getCarReadyToUse();
		VehicleList readyLKW = vl.getLKWReadyToUse();


		System.out.println(readyCar);
		System.out.println(readyLKW);

	}
}
