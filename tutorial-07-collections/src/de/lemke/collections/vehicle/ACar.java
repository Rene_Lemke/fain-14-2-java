package de.lemke.collections.vehicle;

public abstract class ACar extends AVehicle {

	public ACar() {

		super();
	}

	public ACar(int km, Status s) {

		super(km, s);
	}
}
