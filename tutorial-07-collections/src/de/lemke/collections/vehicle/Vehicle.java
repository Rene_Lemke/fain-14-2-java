package de.lemke.collections.vehicle;

public interface Vehicle {

	public void move(int km);

	public void setReadyToUse();
	public void setMaintenance();

	public Status getCurrentState();
}
