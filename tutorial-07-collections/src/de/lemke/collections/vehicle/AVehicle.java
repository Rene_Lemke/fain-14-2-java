package de.lemke.collections.vehicle;

public abstract class AVehicle implements Vehicle {

	protected int kmStand;
	protected Status status;

	public AVehicle() {

		this.kmStand=0;
		this.status = Status.FERTIG;
	}

	public AVehicle(int km, Status s) {

		this.kmStand=km;
		this.status=s;
	}

	@Override
	public void move(int km) {

		this.kmStand += km;
	}

	@Override
	public String toString() {

		return String.format("[%20s] %10d %14s", this.getClass().getSimpleName(), this.kmStand, this.status);
	}

	@Override
	public void setReadyToUse() {

		this.status = Status.FERTIG;
	}

	@Override
	public void setMaintenance() {

		this.status = Status.WARTUNG;
	}

	@Override
	public Status getCurrentState() {

		return this.status;
	}
}
